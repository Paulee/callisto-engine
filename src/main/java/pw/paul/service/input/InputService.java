/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.service.input;

import lombok.Getter;
import pw.paul.logging.Logger;
import pw.paul.main.game.Game;

import pw.paul.math.vec.Vec2i;
import pw.paul.render.canvas.element.IElement;
import pw.paul.service.*;
import pw.paul.service.event.impl.*;

import java.util.*;

import static org.lwjgl.glfw.GLFW.*;

public class InputService implements Service {

  @Getter
  private final Map<String, List<Integer>> keys = new HashMap<>();

  public Vec2i mouse = new Vec2i(0, 0);

  public void init() {
    long id = Game.get().getGameID();

    glfwSetKeyCallback(id, (long window, int key, int scanCode, int action, int mods) -> {
      if (action == GLFW_RELEASE) {
        ServiceHandler.getService().eventService().fire(new KeyReleaseEvent(key));
      } else if (action == GLFW_PRESS) {
        ServiceHandler.getService().eventService().fire(new KeyPressEvent(key));
      }
    });

    glfwSetCursorPosCallback(id, (long window, double mouseX, double mouseY) -> {
      this.mouse.setX((int) mouseX);
      this.mouse.setY((int) mouseY);

      ServiceHandler.getService().eventService().fire(new MouseMoveEvent(mouseX, mouseY));
    });

    glfwSetMouseButtonCallback(id, (long window, int btn, int action, int mods) -> {
      if (action == GLFW_RELEASE) {
        ServiceHandler.getService().eventService().fire(new MouseReleaseEvent(btn));
      } else if (action == GLFW_PRESS) {
        if (Game.get().canvas() != null) {
          Game.get().canvas().getElements().stream().filter(IElement::hovered)
            .forEach(element -> element.click(btn));
        }

        ServiceHandler.getService().eventService().fire(new MousePressEvent(btn));
      }
    });
  }

  public void register(String keyName, Integer... keys) {
    this.keys.computeIfAbsent(keyName, s -> new ArrayList<>(Arrays.asList(keys)));
    Logger.debug("Registered new Keyset as '" + keyName + "' " + Arrays.toString(keys));
  }

}
