package pw.paul.service;

import lombok.Getter;
import pw.paul.assets.AssetsService;
import pw.paul.service.event.EventService;
import pw.paul.service.input.InputService;
import pw.paul.utils.archive.ArchiveService;

import java.util.*;

/*
    created by Tammo
    https://www.youtube.com/channel/UCdBqFeDuzfL-_48yVRJRMRw
 */

public class ServiceHandler {

  @Getter
  private static final ServiceHandler service = new ServiceHandler();

  @Getter
  private final List<Service> services = new ArrayList<>();

  public void add(Service service) {
    this.getServices().add(service);
  }

  public void initAll() {
    this.getServices().forEach(Service::init);
  }

  public <T extends Service> T get(Class<T> service) {
    return service.cast(
      this.getServices().stream().filter(s -> s.getClass().equals(service)).findFirst()
        .orElse(null));
  }

  public EventService eventService() {
    return this.get(EventService.class);
  }

  public InputService inputService() {
    return this.get(InputService.class);
  }

  public AssetsService assetsService() {
    return this.get(AssetsService.class);
  }

  public ArchiveService archiveService() {
    return this.get(ArchiveService.class);
  }

}
