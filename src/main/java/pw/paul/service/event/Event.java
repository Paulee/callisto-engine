/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.service.event;

import lombok.Data;

@Data
public class Event {

  private boolean canceled;

}
