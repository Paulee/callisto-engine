/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.service.event.impl;

import lombok.*;
import pw.paul.service.event.Event;

@RequiredArgsConstructor
public class MouseReleaseEvent extends Event {

  @Getter
  private final int button;

}
