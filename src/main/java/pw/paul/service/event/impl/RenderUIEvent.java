/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.service.event.impl;

import lombok.*;
import pw.paul.service.event.Event;

@RequiredArgsConstructor
public class RenderUIEvent extends Event {

  @Getter
  private final int width, height;

}
