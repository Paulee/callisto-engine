/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.service.event.impl;

import lombok.*;
import pw.paul.service.event.Event;

@AllArgsConstructor
public class UpdateEvent extends Event {

  @Getter
  private float delta;

}
