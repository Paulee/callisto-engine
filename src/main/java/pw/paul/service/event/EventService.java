package pw.paul.service.event;

import lombok.AllArgsConstructor;
import pw.paul.service.Service;

import java.util.concurrent.CopyOnWriteArrayList;

import java.util.*;
import java.util.stream.*;
import java.lang.reflect.*;

/*
    created by Tammo
    https://www.youtube.com/channel/UCdBqFeDuzfL-_48yVRJRMRw
 */
public class EventService implements Service {

  private final HashMap<Class, CopyOnWriteArrayList<MethodData>> registeredMethods = new HashMap<>();

  public void register(Object target) {
    Arrays.stream(target.getClass().getDeclaredMethods()).filter(
      method -> method.getParameterCount() > 0 && method
        .isAnnotationPresent(EventTarget.class)).forEach(method -> {
      final Class<?> eventClass = method.getParameterTypes()[0];
      final MethodData data = new MethodData(target, method);

      if (this.registeredMethods.containsKey(eventClass)) {
        this.registeredMethods.get(eventClass).add(data);
      } else {
        this.registeredMethods.put(
          eventClass,
          new CopyOnWriteArrayList<>(Stream.of(data).collect(Collectors.toList()))
        );
      }
    });
  }

  public void unregister(Object target) {
    this.registeredMethods.keySet().forEach(
      event -> this.registeredMethods.get(event).stream()
        .filter(methodData -> methodData.source.equals(target))
        .forEach(methodData -> this.registeredMethods.get(event).remove(methodData)));
  }

  public void fire(Event event) {
    CopyOnWriteArrayList<MethodData> methods = this.registeredMethods
      .get(event.getClass());

    if (methods == null || event.isCanceled()) {
      return;
    }

    methods.stream().peek(methodData -> methodData.target.setAccessible(true))
      .forEach(methodData -> {
        try {
          methodData.target.invoke(methodData.source, event);
        } catch (IllegalAccessException | InvocationTargetException e) {
          e.printStackTrace();
        }
      });
  }

  @AllArgsConstructor
  private final class MethodData {

    private Object source;
    private Method target;

  }

}
