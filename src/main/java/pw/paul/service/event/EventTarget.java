/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.service.event;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventTarget {}
