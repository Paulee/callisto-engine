/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.logging;

import lombok.*;

@RequiredArgsConstructor
public enum LogStatus {

  EXCEPTION(95),
  DEBUG(96),
  ERROR(31),
  WARNING(93),
  INFO(92);

  @Getter
  private final int ansi;

}
