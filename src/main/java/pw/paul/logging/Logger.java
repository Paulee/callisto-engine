/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.logging;

public class Logger {

  public static boolean disableSaveFile, disableDebugPrints, disableHardwarePrints,
    disableGLPrints;

  public static void debug(Object obj) {
    log(obj, LogStatus.DEBUG);
  }

  public static void error(Object obj) {
    log(obj, LogStatus.ERROR);
  }

  public static void warning(Object obj) {
    log(obj, LogStatus.WARNING);
  }

  public static void info(Object obj) {
    log(obj, LogStatus.INFO);
  }

  public static void exception(String exceptionMessage) {
    log(exceptionMessage, LogStatus.EXCEPTION);
    throw new IllegalStateException(exceptionMessage);
  }

  private static void log(Object obj, LogStatus status) {
    String log = "\u001b[37m[\u001b[" + status.getAnsi() + "m" + status
      .name() + "\u001b[37m]\u001b[0m | " + obj.toString();
    if (!disableSaveFile) {
      System.out.println("TODO Later");
    }

    if (status.equals(LogStatus.DEBUG) && disableDebugPrints) {
      return;
    }

    System.out.println(log);
  }
}
