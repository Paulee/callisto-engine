/*
 * Copyright by Paul 22|5|2020
 */

package pw.paul.ecs.entity;

import pw.paul.ecs.entity.component.annotations.*;
import pw.paul.utils.profiler.Profiler;

import static org.lwjgl.glfw.GLFW.*;

//@Animated(rightShift = 96, fps = 20)
@SpriteConf(name = "player_sprite", textureName = "player")
public class Player extends Entity{

    public Player(){
        this.pos(96, 16);
    }

    public void update(float delta) {

        //System.out.println("Player: " + this.getAabb().toString());
        Direction direction = Direction.NONE;

        if(isKeyHold("up"))
            direction = Direction.UP;
        if(isKeyHold("down"))
            direction = Direction.DOWN;
        if(isKeyHold("left"))
            direction = Direction.LEFT;
        if(isKeyHold("right"))
            direction = Direction.RIGHT;

        Profiler.get().start("move", "Entity Movement");
        this.move(25 * delta, direction);
        Profiler.get().end("move");

        this.setAabb(new AxisAlignBB(this.getPosition().getX(), this.getPosition().getY() - 16, 16, 16, this.getScale().getX(), this.getScale().getY()));
    }

    @Override
    public void onCollide(Entity collidingEntity) {
    }
}




