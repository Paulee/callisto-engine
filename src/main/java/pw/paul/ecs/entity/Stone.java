/*
 * Copyright by Paul 29|5|2020
 */

package pw.paul.ecs.entity;

import pw.paul.ecs.entity.component.annotations.*;
import pw.paul.math.vec.Vec2i;

@Static
@SpriteConf(name = "stone_sprite", textureName = "stone")
public class Stone extends Entity{

    public Stone(Vec2i vec){
        this.pos(vec.getX(), vec.getY());

        this.initEntity();
    }

    public void update(float delta) {
        //System.out.println("Stone: " + getAabb().toString());
    }
}
