/*
 * Copyright by Paul 31|5|2020
 */

package pw.paul.ecs.entity.component.annotations;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Static {}
