/*
 * Copyright by Paul 1|6|2020
 */

package pw.paul.ecs.entity.component.annotations;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Passability {

    boolean passible() default true;

}
