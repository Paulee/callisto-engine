/*
 * Copyright by Paul 23|5|2020
 */

package pw.paul.ecs.entity.component.annotations;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Animated {

    int rightShift();
    float fps() default 30;

}
