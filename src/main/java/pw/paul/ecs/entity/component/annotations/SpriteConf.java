/*
 * Copyright by Paul 23|5|2020
 */

package pw.paul.ecs.entity.component.annotations;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SpriteConf {

    String name();
    String textureName();
    int color() default -1;
    float[] offset() default {};

}