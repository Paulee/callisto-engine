/*
 * Copyright by Paul 29|5|2020
 */

package pw.paul.ecs.entity;


import lombok.Getter;
import pw.paul.math.vec.Vec2f;
import pw.paul.math.vec.Vec2i;

public class AxisAlignBB {

    public static AxisAlignBB NONE = new AxisAlignBB(0, 0, 0, 0, 0, 0);

    @Getter
    private final float minX, minY, maxX, maxY, scaleX, scaleY;

    @Getter
    private Vec2f center;

    public AxisAlignBB(float minX, float minY, float maxX, float maxY, float scaleX, float scaleY){
        this.maxX = maxX * scaleX;
        this.maxY = maxY * scaleY;
        this.minX = minX - (this.maxX * .5f);
        this.minY = minY - (this.maxY * .5f);
        this.scaleX = scaleX;
        this.scaleY = scaleY;

        this.center = new Vec2f(this.minX + (this.maxX * .5f), this.minY + (this.maxY * .5f));
    }

    public boolean isColliding(AxisAlignBB axisAlignBB){
        float halfWidth = this.maxX * .5f;
        float halfHeight = this.maxY * .5f;

        float halfWidthOther = axisAlignBB.maxX * .5f;
        float halfHeightOther = axisAlignBB.maxY * .5f;

        float dx = Math.abs(this.center.getX() - axisAlignBB.center.getX());
        float dy = Math.abs(this.center.getY() - axisAlignBB.center.getY());

        return dx < (halfWidth + halfWidthOther) && dy < (halfHeight + halfHeightOther);
    }

    @Override
    public String toString() {
        return "MinX: " + this.minX + " MinY: " + this.minY + " MaxX: " + this.maxX + " MaxY: " + this.maxY;
    }
}
