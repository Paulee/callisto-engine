/*
 * Copyright by Paul 20|5|2020
 */

package pw.paul.ecs.entity;


import lombok.Getter;
import lombok.Setter;
import pw.paul.ecs.Environment;
import pw.paul.ecs.entity.component.annotations.*;
import pw.paul.main.Callisto;
import pw.paul.math.Matrix4f;
import pw.paul.math.vec.Vec2f;
import pw.paul.render.texture.Texture2D;
import pw.paul.utils.DeletableObject;

import java.util.UUID;

import pw.paul.render.texture.sprite.*;

public abstract class Entity extends Callisto {

    private final String id;

    @Getter
    private Sprite sprite;

    @Getter
    @Setter
    private AxisAlignBB aabb = AxisAlignBB.NONE;

    @Getter
    private Vec2f position = new Vec2f(0, 0), scale = new Vec2f(3, 3), fixPoint = new Vec2f(.5f, .5f);

    private Vec2f temp;

    @Getter
    private final Matrix4f matrix = new Matrix4f();

    private byte dirty = 1;

    private float rotation;

    public Entity(){
        this.id = UUID.randomUUID().toString();

        this.initEntity();
    }

    protected final void initEntity(){
        this.generateSprite();
        this.generateBox();
    }

    public abstract void update(float delta);

    private void generateSprite() {
        if(!this.hasSprite())
            return;

        SpriteConf spriteAnno = this.getClass().getAnnotation(SpriteConf.class);

        this.sprite = new Sprite(spriteAnno.name(), DeletableObject.get(spriteAnno.textureName(), Texture2D.class), spriteAnno.color());

        if(spriteAnno.offset().length == 4)
            sprite.offset(spriteAnno.offset());

        if(this.isAnimated()){
            Animated animatedAnno = this.getClass().getAnnotation(Animated.class);
            this.sprite = new AnimatedSprite(spriteAnno.name(), sprite, animatedAnno.rightShift(), animatedAnno.fps());
            this.sprite.add();
        }
    }

    private void generateBox(){
        this.aabb = new AxisAlignBB(this.position.getX(), this.position.getY(), this.sprite.getTexture().getWidth(), this.sprite.getTexture().getHeight(), this.scale.getX(), this.scale.getY());
    }

    public boolean updated(){
        return this.dirty == 1;
    }

    public void updateMatrix(){
        this.matrix.transform(this.position, this.rotation);
        this.dirty = 0;
    }

    protected void move(float speed, Direction direction){
        if(this.temp == null)
            this.temp = this.position;

        this.add(this.position.getX() - this.lerp(this.position.getX(), temp.getX(), speed), this.position.getY() - this.lerp(this.position.getY(), temp.getY(), speed));

        if(this.position.distance(this.temp) <= 0.03) {
            this.position = new Vec2f(Math.round(this.position.getX()), Math.round(this.position.getY()));

            int xForce = direction.getForce().getX() * Environment.get().enviromentGrid.getX();
            int yForce = direction.getForce().getY() * Environment.get().enviromentGrid.getY();

            if(Environment.get().predictFromAABB(Environment.get().posFromAABB(this).add(direction.getForce().getX(), direction.getForce().getY())) != null)
                return;

            this.temp = temp.add(xForce, yForce);
        }
    }

    private float lerp(float start, float end, float epsilon){
        return start + epsilon * (start - end);
    }

    private void add(float dx, float dy){
        this.pos(this.position.getX() + dx, this.position.getY() + dy);
    }

    public void pos(float x, float y){
        this.dirty = 1;
        this.position = new Vec2f(x, y);
    }

    public void scale(float x, float y){
        this.dirty = 1;
        this.scale = new Vec2f(x, y);
    }

    public void fixPoint(float x, float y){
        this.dirty = 1;
        this.fixPoint = new Vec2f(x, y);
    }

    public void rotate(float rotation){
        this.dirty = 1;
        this.rotation = rotation;
    }

    public boolean hasSprite(){
        return this.getClass().isAnnotationPresent(SpriteConf.class);
    }

    public boolean isAnimated(){
        return this.getClass().isAnnotationPresent(Animated.class);
    }

    public boolean isPassable(){
        return this.getClass().isAnnotationPresent(Passability.class) && this.getClass().getAnnotation(Passability.class).passible();
    }

    public boolean isColliding(Entity entityIn){
        if(this.equals(entityIn))
            return false;

        return this.aabb.isColliding(entityIn.aabb);
    }

    public boolean isStatic(){
        return this.getClass().isAnnotationPresent(Static.class);
    }

    public void onCollide(Entity collidingEntity){}
}
