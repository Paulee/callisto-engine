/*
 * Copyright by Paul 1|6|2020
 */

package pw.paul.ecs.entity;

import pw.paul.ecs.entity.component.annotations.*;

@Static
@Passability(passible = true)
@SpriteConf(name = "bush_sprite", textureName = "bush")
public class Bush extends Entity{

    public Bush(){
        this.pos(0, 48);

        this.initEntity();
    }

    public void update(float delta) {

    }
}
