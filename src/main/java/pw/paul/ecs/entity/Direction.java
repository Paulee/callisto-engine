/*
 * Copyright by Paul 31|5|2020
 */

package pw.paul.ecs.entity;

import lombok.*;
import pw.paul.math.vec.Vec2i;

@RequiredArgsConstructor
public enum Direction {

    NONE(new Vec2i(0, 0)),
    UP(new Vec2i(0, 1)),
    DOWN(new Vec2i(0, -1)),
    LEFT(new Vec2i(-1, 0)),
    RIGHT(new Vec2i(1, 0));

    @Getter
    private final Vec2i force;

}
