/*
 * Copyright by Paul 29|7|2020
 */

package pw.paul.sound;

import pw.paul.logging.Logger;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.ALC10.*;

public final class SoundHelper {

  public static void checkALCError(long device) {
    int alcError = alcGetError(device);
    if (alcError != ALC_NO_ERROR) {
      Logger.debug("Error(ALC): " + alcError + " | " + alcGetString(
        device,
        alcError
      ));
    }
  }

  public static void checkALError() {
    int alError = alGetError();
    if (alError != AL_NO_ERROR) {
      Logger.debug("Error(AL): " + alError + " | " + alGetString(alError));
    }
  }
}
