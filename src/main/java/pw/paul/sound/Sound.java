/*
 * Copyright by Paul 28|7|2020
 */

package pw.paul.sound;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import lombok.Getter;
import org.lwjgl.BufferUtils;
import org.lwjgl.stb.STBVorbisInfo;
import pw.paul.logging.Logger;
import pw.paul.utils.DeletableObject;
import pw.paul.utils.buffer.Buffers;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.stb.STBVorbis.*;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Sound extends DeletableObject {

  @Getter
  private final int soundID, sourceID;

  public Sound(String name) {
    super(name);

    this.soundID = alGenBuffers();
    this.sourceID = alGenSources();

    try (STBVorbisInfo info = STBVorbisInfo.malloc()) {

      ShortBuffer pcm = this.read(name, info);

      if (pcm == null) {
        this.delete();
        Logger.debug("can't create ShortBuffer for " + name);
        return;
      }

      alBufferData(this.soundID, info.channels() == 1 ? AL_FORMAT_MONO16 :
        AL_FORMAT_STEREO16, pcm, info.sample_rate());

      alSourcei(this.sourceID, AL_BUFFER, this.soundID);

      this.add();
    }
  }

  public void play(float gain, boolean loop) {
    alSourcef(this.sourceID, AL_GAIN, gain);
    alSourcei(this.sourceID, AL_LOOPING, loop ? AL_TRUE : AL_FALSE);
    alSourcePlay(this.sourceID);
  }

  public void pause() {
    alSourcePause(this.sourceID);
  }

  public void stop() {
    alSourceStop(this.sourceID);
  }

  private ShortBuffer read(String name, STBVorbisInfo info) {
    ByteBuffer vorbis = Buffers.ioToBuffer(name);

    IntBuffer error = BufferUtils.createIntBuffer(1);
    assert vorbis != null;
    long decoder = stb_vorbis_open_memory(vorbis, error, null);

    if (decoder == NULL) {
      Logger.debug("Decoder seems to be null!(" + name + ")");
      return null;
    }

    stb_vorbis_get_info(decoder, info);

    int channels = info.channels();

    ShortBuffer pcm =
      BufferUtils
        .createShortBuffer(stb_vorbis_stream_length_in_samples(decoder) * channels);

    stb_vorbis_get_samples_short_interleaved(decoder, channels, pcm);
    stb_vorbis_close(decoder);
    return pcm;
  }

  protected void delete() {
    this.stop();
    alDeleteBuffers(this.soundID);
    alDeleteSources(this.sourceID);
  }
}
