package pw.paul.math.vec;

import lombok.*;

import java.util.StringJoiner;

@AllArgsConstructor
public class Vec4f {

  @Getter
  @Setter
  private float w, x, y, z;

  public Vec4f add(Vec4f vec) {
    return this.add(vec.w, vec.x, vec.y, vec.z);
  }

  public Vec4f add(float w, float x, float y, float z) {
    return new Vec4f(this.w + w, this.x + x, this.y + y, this.y + z);
  }

  public Vec4f subtract(Vec4f vec) {
    return this.subtract(vec.x, vec.x, vec.y, vec.z);
  }

  public Vec4f subtract(float w, float x, float y, float z) {
    return this.add(-w, -x, -y, -z);
  }

  public Vec4f multiply(Vec4f vec) {
    return this.multiply(vec.w, vec.x, vec.y, vec.z);
  }

  public Vec4f multiply(float w, float x, float y, float z) {
    return new Vec4f(this.w * w, this.x * x, this.y * y, this.z * z);
  }

  public Vec4f divide(Vec4f vec) {
    return this.divide(vec.w, vec.x, vec.y, vec.z);
  }

  public Vec4f divide(float w, float x, float y, float z) {
    return new Vec4f(this.w / w, this.x / x, this.y / y, this.z / z);
  }

  public float distance(Vec4f vec) {
    return this.distance(vec.w, vec.x, vec.y, vec.z);
  }

  public float distance(float w, float x, float y, float z) {

    float dw = this.w - w;
    float dx = this.x - x;
    float dy = this.y - y;
    float dz = this.z - z;

    return (float) Math.sqrt(dw * dw + dx * dx + dy * dy + dz * dz);
  }

  public Vec4f asUnit() {
    float magnitude = (float) Math
      .sqrt(this.w * this.w + this.x * this.x + this.y * this.y + this.z * this.z);

    return magnitude == 0 ? new Vec4f(0, 0, 0, 0)
      : new Vec4f(this.w / magnitude, this.x / magnitude, this.y / magnitude,
        this.z / magnitude
      );
  }

  public float length() {
    return this.distance(0, 0, 0, 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", this.getClass().getSimpleName() + " [", "]")
      .add("w=" + this.w)
      .add("x=" + this.x)
      .add("y=" + this.y)
      .add("z=" + this.z)
      .toString();
  }

  @Override
  public boolean equals(Object obj) {
      if (!(obj instanceof Vec4f)) {
          return false;
      }

    return this.w == ((Vec4f) obj).w && this.x == ((Vec4f) obj).x && this.y == ((Vec4f) obj).y && this.z == ((Vec4f) obj).z;
  }

}
