package pw.paul.math.vec;

import lombok.*;

import java.util.StringJoiner;

@AllArgsConstructor
public class Vec3f {

  @Getter
  @Setter
  private float x, y, z;

  public Vec3f add(Vec3f vec) {
    return this.add(vec.x, vec.y, vec.z);
  }

  public Vec3f add(float x, float y, float z) {
    return new Vec3f(this.x + x, this.y + y, this.y + z);
  }

  public Vec3f subtract(Vec3f vec) {
    return this.subtract(vec.x, vec.y, vec.z);
  }

  public Vec3f subtract(float x, float y, float z) {
    return this.add(-x, -y, -z);
  }

  public Vec3f multiply(Vec3f vec) {
    return this.multiply(vec.x, vec.y, vec.z);
  }

  public Vec3f multiply(float x, float y, float z) {
    return new Vec3f(this.x * x, this.y * y, this.z * z);
  }

  public Vec3f divide(Vec3f vec) {
    return this.divide(vec.x, vec.y, vec.z);
  }

  public Vec3f divide(float x, float y, float z) {
    return new Vec3f(this.x / x, this.y / y, this.z / z);
  }

  public float distance(Vec3f vec) {
    return this.distance(vec.x, vec.y, vec.z);
  }

  public float distance(float x, float y, float z) {

    float dx = this.x - x;
    float dy = this.y - y;
    float dz = this.z - z;

    return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
  }

  public Vec3f asUnit() {
    float magnitude = (float) Math
      .sqrt(this.x * this.x + this.y * this.y + this.z * this.z);

    return magnitude == 0 ? new Vec3f(0, 0, 0)
      : new Vec3f(this.x / magnitude, this.y / magnitude, this.z / magnitude);
  }

  public float length() {
    return this.distance(0, 0, 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", this.getClass().getSimpleName() + " [", "]")
      .add("x=" + this.x)
      .add("y=" + this.y)
      .add("z=" + this.z)
      .toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Vec3f)) {
      return false;
    }

    return this.x == ((Vec3f) obj).x && this.y == ((Vec3f) obj).y && this.z == ((Vec3f) obj).z;
  }

}
