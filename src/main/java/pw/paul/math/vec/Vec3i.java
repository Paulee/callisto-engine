package pw.paul.math.vec;

import lombok.*;

import java.util.StringJoiner;

@AllArgsConstructor
public class Vec3i {

  @Getter
  @Setter
  private int x, y, z;

  public Vec3i add(Vec3i vec) {
    return this.add(vec.x, vec.y, vec.z);
  }

  public Vec3i add(int x, int y, int z) {
    return new Vec3i(this.x + x, this.y + y, this.y + z);
  }

  public Vec3i subtract(Vec3i vec) {
    return this.subtract(vec.x, vec.y, vec.z);
  }

  public Vec3i subtract(int x, int y, int z) {
    return this.add(-x, -y, -z);
  }

  public Vec3i multiply(Vec3i vec) {
    return this.multiply(vec.x, vec.y, vec.z);
  }

  public Vec3i multiply(int x, int y, int z) {
    return new Vec3i(this.x * x, this.y * y, this.z * z);
  }

  public Vec3i divide(Vec3i vec) {
    return this.divide(vec.x, vec.y, vec.z);
  }

  public Vec3i divide(int x, int y, int z) {
    return new Vec3i(this.x / x, this.y / y, this.z / z);
  }

  public float distance(Vec3i vec) {
    return this.distance(vec.x, vec.y, vec.z);
  }

  public float distance(float x, float y, float z) {

    float dx = this.x - x;
    float dy = this.y - y;
    float dz = this.z - z;

    return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
  }

  public float length() {
    return this.distance(0, 0, 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", this.getClass().getSimpleName() + " [", "]")
      .add("x=" + this.x)
      .add("y=" + this.y)
      .add("z=" + this.z)
      .toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Vec3i)) {
      return false;
    }

    return this.x == ((Vec3i) obj).x && this.y == ((Vec3i) obj).y && this.z == ((Vec3i) obj).z;
  }

}
