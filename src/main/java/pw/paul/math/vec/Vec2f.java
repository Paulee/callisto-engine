package pw.paul.math.vec;

import lombok.*;

import java.util.StringJoiner;

@AllArgsConstructor
public class Vec2f {

  @Getter
  @Setter
  private float x, y;

  public Vec2f add(Vec2f vec) {
    return this.add(vec.x, vec.y);
  }

  public Vec2f add(float x, float y) {
    return new Vec2f(this.x + x, this.y + y);
  }

  public Vec2f subtract(Vec2f vec) {
    return this.subtract(vec.x, vec.y);
  }

  public Vec2f subtract(float x, float y) {
    return this.add(-x, -y);
  }

  public Vec2f multiply(Vec2f vec) {
    return this.multiply(vec.x, vec.y);
  }

  public Vec2f multiply(float x, float y) {
    return new Vec2f(this.x * x, this.y * y);
  }

  public Vec2f divide(Vec2f vec) {
    return this.divide(vec.x, vec.y);
  }

  public Vec2f divide(float x, float y) {
    return new Vec2f(this.x / x, this.y / y);
  }

  public float distance(Vec2f vec) {
    return this.distance(vec.x, vec.y);
  }

  public float distance(float x, float y) {
    return (float) Math.hypot(this.x - x, this.y - y);
  }

  public Vec2f asUnit() {
    float magnitude = (float) Math.hypot(this.x, this.y);

    return magnitude == 0 ? new Vec2f(0, 0)
      : new Vec2f(this.x / magnitude, this.y / magnitude);
  }

  public float length() {
    return this.distance(0, 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", this.getClass().getSimpleName() + " [", "]")
      .add("x=" + this.x)
      .add("y=" + this.y)
      .toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Vec2f)) {
      return false;
    }

    return this.x == ((Vec2f) obj).x && this.y == ((Vec2f) obj).y;
  }
}
