package pw.paul.math.vec;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.StringJoiner;

@AllArgsConstructor
public class Vec2i {

  @Getter
  @Setter
  private int x, y;

  public Vec2i add(Vec2i vec) {
    return this.add(vec.x, vec.y);
  }

  public Vec2i add(int x, int y) {
    return new Vec2i(this.x + x, this.y + y);
  }

  public Vec2i subtract(Vec2i vec) {
    return this.subtract(vec.x, vec.y);
  }

  public Vec2i subtract(int x, int y) {
    return this.add(-x, -y);
  }

  public Vec2i multiply(Vec2i vec) {
    return this.multiply(vec.x, vec.y);
  }

  public Vec2i multiply(int x, int y) {
    return new Vec2i(this.x * x, this.y * y);
  }

  public Vec2i divide(Vec2i vec) {
    return this.divide(vec.x, vec.y);
  }

  public Vec2i divide(int x, int y) {
    return new Vec2i(this.x / x, this.y / y);
  }

  public float distance(Vec2i vec) {
    return this.distance(vec.x, vec.y);
  }

  public float distance(float x, float y) {
    return (float) Math.hypot(this.x - x, this.y - y);
  }

  public float length() {
    return this.distance(0, 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", this.getClass().getSimpleName() + " [", "]")
      .add("x=" + this.x)
      .add("y=" + this.y)
      .toString();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Vec2i)) {
      return false;
    }

    return this.x == ((Vec2i) obj).x && this.y == ((Vec2i) obj).y;
  }
}
