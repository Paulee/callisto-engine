/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.math;

import pw.paul.math.vec.Vec2f;

import java.nio.FloatBuffer;
import java.util.Arrays;

public class Matrix4f {

  private final float[][] matrix = new float[4][4];

  public Matrix4f() {
    this.identity();
  }

  public void identity() {
    Arrays.stream(this.matrix).forEach(m -> Arrays.fill(m, 0));

    for ( int i = 0; i < 4; i++ ) {
      this.matrix[i][i] = 1;
    }
  }

  public static Matrix4f ortho(
    float left, float right, float bottom, float top, float near, float far
  ) {
    Matrix4f matrix = new Matrix4f();

    float[] mData = new float[]{
      right - left, top - bottom, far - near, right + left, top + bottom, far + near
    };

    for ( int i = 0; i < 3; i++ ) {
      matrix.matrix[i][i] = 2 / mData[i];
      matrix.matrix[3][i] = -mData[i + 3] / mData[i];
    }

    return matrix;
  }

  public void transform(Vec2f vec, float rotation) {
    float radians = (float) Math.toRadians(rotation);
    float cos = (float) Math.cos(radians);
    float sin = (float) Math.sin(radians);

    this.matrix[0][0] = cos;
    this.matrix[0][1] = sin;
    this.matrix[0][3] = vec.getX();

    this.matrix[1][0] = -sin;
    this.matrix[1][1] = cos;
    this.matrix[1][3] = vec.getY();
  }

  public void apply(FloatBuffer buffer) {
    for ( int i = 0; i < 4; i++ ) {
      for ( int j = 0; j < 4; j++ ) {
        buffer.put(this.matrix[i][j]);
      }

    }
    buffer.flip();
  }
}
