/*
 * Copyright by Paul 30|7|2020
 */

package pw.paul.enviroment;

import pw.paul.ecs.entity.Entity;
import pw.paul.main.game.Game;
import pw.paul.math.vec.Vec2f;
import pw.paul.math.vec.Vec2i;
import pw.paul.service.ServiceHandler;
import pw.paul.service.event.EventTarget;
import pw.paul.service.event.impl.LoadEnviromentEvent;
import pw.paul.service.event.impl.UpdateEvent;

import java.util.ArrayList;
import java.util.List;

public class Environment {

  private static final Environment enviroment = new Environment();

  private Environment() {}

  public Vec2i enviromentGrid = new Vec2i(16 * 3, 16 * 3);

  private final List<Entity> entities = new ArrayList<>();

  public void add(Entity entityIn) {
    this.entities.add(entityIn);
  }

  public void loadEnviroment() {
    Game.get().setCanvas(null);
    ServiceHandler.getService().eventService().fire(new LoadEnviromentEvent());
  }

  @EventTarget
  public void onUpdate(UpdateEvent e) {
    this.entities.forEach(entity -> entity.update(e.getDelta()));
    this.entities.stream().filter(entity -> !entity.isStatic()).forEach(
      entity -> this.entities.stream().filter(Entity::isPassable).forEach(entity1 -> {
        if (entity.isColliding(entity1)) {
          entity.onCollide(entity1);
        }

      }));
  }

  public List<Entity> entities() {
    return this.entities;
  }

  public Entity get(Class<? extends Entity> clazz) {
    return this.entities.stream().filter(entity -> entity.getClass().equals(clazz))
      .findFirst().orElse(null);
  }

  public Vec2i pos(Entity entityIn) {
    return new Vec2i(
      Math.round(entityIn.getPosition().getX()) / this.enviromentGrid.getX(),
      Math.round(entityIn.getPosition().getY()) / this.enviromentGrid.getY()
    );
  }

  public Vec2i pos(Vec2f vec) {
    return new Vec2i(
      Math.round(vec.getX()) / this.enviromentGrid.getX(),
      Math.round(vec.getY()) / this.enviromentGrid.getY()
    );
  }

  public void setGrid(int dx, int dy) {
    this.enviromentGrid = new Vec2i(dx, dy);
  }

  public static Environment get() {
    return enviroment;
  }
}
