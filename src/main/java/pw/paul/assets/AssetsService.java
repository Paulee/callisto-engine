/*
 * Copyright by Paul 11|5|2020
 */

package pw.paul.assets;

import pw.paul.logging.Logger;
import pw.paul.render.shader.Shader;
import pw.paul.render.texture.Texture2D;
import pw.paul.service.Service;
import pw.paul.sound.Sound;

import java.io.InputStream;
import java.util.*;

public class AssetsService implements Service {

  private final Map<String, Byte> objectAssets = new HashMap<>();

  public void loadAsset(String name, byte type) {
    if (this.objectAssets.containsKey(name) && this.objectAssets
      .get(name) == type || type > 2) {
      return;
    }

    long start = System.nanoTime();

    switch (type) {
      case 0 -> new Texture2D(name);
      case 1 -> new Shader(name);
      case 2 -> new Sound(name);
    }
    Logger
      .debug("loading Asset(type= " + this.getType(type) + "): " + name + " in " + (System
        .nanoTime() - start) / 1000000f + "ms");

    this.objectAssets.put(name, type);
  }

  private String getType(byte type) {
    return switch (type) {
      case 0 -> "Texture";
      case 1 -> "Shader";
      case 2 -> "Sound";
      default -> "Unknown Asset: " + type;
    };
  }

  public InputStream createStream(String resourcePath) {
    return this.getClass().getClassLoader().getResourceAsStream(resourcePath);
  }

}
