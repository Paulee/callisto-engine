/*
 * Copyright by Paul 15|4|2020
 */

package pw.paul.main.game;

import lombok.Getter;
import pw.paul.logging.Logger;
import pw.paul.service.Service;
import pw.paul.service.ServiceHandler;

import java.util.HashMap;
import java.util.Map;

public class SettingPool {

  @Getter
  private String gameTitle = "Callisto2D", icon = "assets/textures/icon.png";

  private int width = 800, height = 600, frameRate = 60;

  @Getter
  private boolean vSync = true, fullScreen;

  private float opacity = 1;

  @Getter
  private final Map<Integer, Integer> hints = new HashMap<>();

  public int width() {
    return this.width;
  }

  public int height() {
    return this.height;
  }

  public int frameRate() {
    return this.frameRate;
  }

  public float opacity() {
    return this.opacity;
  }

  public SettingPool title(String gameTitle) {
    this.gameTitle = gameTitle;
    return this;
  }

  public SettingPool icon(String iconPath) {
    this.icon = iconPath;
    return this;
  }

  public SettingPool dimension(int width, int height) {
    this.width = width;
    this.height = height;
    return this;
  }

  public SettingPool frameRate(int frameRate) {
    this.frameRate = frameRate;
    return this;
  }

  public SettingPool vSync(boolean vSync) {
    this.vSync = vSync;
    return this;
  }

  public SettingPool fullScreen(boolean fullScreen) {
    this.fullScreen = fullScreen;
    return this;
  }

  public SettingPool opacity(float opacity) {
    this.opacity = opacity / 100;
    return this;
  }

  public SettingPool hint(int hint, int value) {
    if (this.hints.containsKey(hint)) {
      Logger.debug(hint + " got added again!");
      return this;
    }
    this.hints.put(hint, value);

    return this;
  }

  public SettingPool service(Service service) {
    ServiceHandler.getService().add(service);
    return this;
  }
}
