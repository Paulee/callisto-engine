/*
 * Copyright by Paul 15|4|2020
 */

package pw.paul.main.game;

import org.lwjgl.openal.AL;
import org.lwjgl.openal.ALC;
import org.lwjgl.openal.ALC10;
import org.lwjgl.opengl.GL;
import pw.paul.assets.AssetsService;
import pw.paul.enviroment.Environment;
import pw.paul.logging.Logger;
import pw.paul.main.Callisto;
import pw.paul.render.canvas.Canvas;
import pw.paul.render.texture.Texture2D;
import pw.paul.service.ServiceHandler;
import pw.paul.service.event.EventService;

import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.util.Arrays;
import java.util.Objects;

import pw.paul.service.input.InputService;
import pw.paul.sound.SoundHelper;
import pw.paul.utils.DeletableObject;
import pw.paul.utils.archive.ArchiveService;
import pw.paul.utils.buffer.Buffers;
import pw.paul.utils.hardware.Hardware;
import pw.paul.utils.profiler.Profiler;

import lombok.*;
import pw.paul.render.*;
import org.lwjgl.glfw.*;
import pw.paul.service.event.impl.*;
import pw.paul.utils.tray.TrayAdapter;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

public class Game {

  @Getter
  private long gameID;

  @Getter
  private int FPS;

  @Getter
  @Setter
  private float frameRate, frameTime;

  private boolean started;

  private final static Game game = new Game();

  private Canvas current;

  private Game() {}

  public void run(SettingPool pool, Object... listener) {
    if (this.started) {
      Logger.debug("You can't call the 'run' Method twice!");
      return;
    }

    if (!glfwInit()) {
      Logger.exception("Unable to initialize GLFW!");
    }

    GLFWErrorCallback.createPrint(System.err).set();

    this.frameRate = pool.frameRate();
    this.frameTime = 1 / this.frameRate;

    pool.getHints().keySet()
      .forEach(hint -> glfwWindowHint(hint, pool.getHints().get(hint)));

    GLFWVidMode vm = Hardware.get().getMode(glfwGetPrimaryMonitor());

    assert vm != null : "Couldn't create VideoMode";

    if (pool.isFullScreen() || (pool.getHints().containsKey(GLFW_DECORATED) && pool
      .getHints().get(GLFW_DECORATED) == GLFW_FALSE)) {
      pool.dimension(vm.width(), vm.height());
    }

    this.gameID = glfwCreateWindow(
      pool.width(), pool.height(), pool.getGameTitle(),
      pool.isFullScreen() ? glfwGetPrimaryMonitor() : NULL, NULL
    );

    if (this.gameID == NULL) {
      Logger.exception("Unable to create Window!");
    }

    this.started = true;

    long device = ALC10.alcOpenDevice((CharSequence) null);

    if (device == NULL) {
      Logger.exception("Unable to open default device!");
    }

    ALC10.alcMakeContextCurrent(ALC10.alcCreateContext(device, (int[]) null));
    AL.createCapabilities(ALC.createCapabilities(device));

    SoundHelper.checkALCError(device);

    ServiceHandler.getService().add(new EventService());
    ServiceHandler.getService().add(new InputService());
    ServiceHandler.getService().add(new AssetsService());
    ServiceHandler.getService().add(new ArchiveService());

    glfwSetWindowOpacity(this.gameID, pool.opacity());
    glfwSetWindowIcon(
      this.gameID, GLFWImage.malloc(1)
        .put(0, GLFWImage.malloc().set(32, 32, Buffers.parse(pool.getIcon()))));

    glfwShowWindow(this.gameID);
    glfwMakeContextCurrent(this.gameID);

    if (!pool.isFullScreen()) {
      glfwSetWindowPos(
        this.gameID, (vm.width() - pool.width()) >> 1,
        (vm.height() - pool.height()) >> 1
      );
    }

    glfwSwapInterval(pool.isVSync() ? 1 : 0);

    GL.createCapabilities();

    if (!Logger.disableHardwarePrints) {
      Hardware.get().specs();
    }

    if (!Logger.disableGLPrints) {
      OpenGLHelper.prints();
    }

    Logger.info("Made with Callisto by Paul");
    Logger.info("https://gitlab.com/Paulee/callisto-engine");

    glfwSetWindowSizeCallback(
      this.gameID, GLFWWindowSizeCallback.create((id, width, height) -> {
        pool.dimension(width, height);
        glViewport(0, 0, width, height);
        ServiceHandler.getService().eventService()
          .fire(new WindowResizeEvent(width, height));
      }));

    ServiceHandler.getService().eventService().register(Environment.get());

    Arrays.stream(listener).distinct()
      .forEach(ServiceHandler.getService().eventService()::register);

    ServiceHandler.getService().initAll();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    new Renderer(pool.width(), pool.height());
    Texture2D.allocateEmptyTexture();

    if (TrayAdapter.isTraySupported() && !TrayAdapter.disableDefaultTray) {
      new TrayAdapter("Callisto")
        .build(Objects.requireNonNull(
          ServiceHandler.getService().assetsService().createStream(
            "assets/textures/icon.png")))
        .seperator()
        .addItem(
          "Close", (actionListener) -> this.close())
        .finish();
    }

    ServiceHandler.getService().eventService().fire(new StartEvent());

    int frames = 0;
    long counter = 0, last = System.nanoTime();
    double unprocessed = 0;

    while (!glfwWindowShouldClose(this.gameID)) {
      boolean canRender = false;

      long start = System.nanoTime(), passed = start - last;
      last = start;

      unprocessed += passed / (double) Callisto.SECOND;
      counter += passed;

      while (unprocessed > this.frameTime) {
        canRender = true;
        unprocessed -= this.frameTime;

        Profiler.get().start("update", "Update");
        this.update(this.frameTime);
        Profiler.get().end("update");

        if (counter >= Callisto.SECOND) {
          this.FPS = frames;
          frames = 0;
          counter = 0;
        }
      }
      if (canRender) {
        this.render(pool.width(), pool.height());
        frames++;
      } else {
        try {
          Thread.sleep(10);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }

    ServiceHandler.getService().eventService().fire(new CloseEvent());

    Texture2D.deleteEmptyTexture();
    DeletableObject.deleteAll();

    for ( TrayIcon tray : SystemTray.getSystemTray().getTrayIcons() ) {
      SystemTray.getSystemTray().remove(tray);
    }

    ALC10.alcCloseDevice(device);

    glfwFreeCallbacks(this.gameID);
    glfwDestroyWindow(this.gameID);
    glfwTerminate();
  }


  private void update(float delta) {
    glfwPollEvents();

    ServiceHandler.getService().eventService().fire(new UpdateEvent(delta));

    SoundHelper.checkALError();
  }

  private void render(int width, int height) {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    ServiceHandler.getService().eventService().fire(new RenderEvent(width, height));
    glClearColor(0, 0, 0, 1);

    OpenGLHelper.checkErrors();

    glfwSwapBuffers(this.gameID);
  }

  public void setCanvas(Canvas canvas) {
    var temp = this.current;
    this.current = canvas;

    if (temp != null) {
      ServiceHandler.getService().eventService().unregister(temp);
    }

    if (this.current != null) {
      ServiceHandler.getService().eventService().register(this.current);
      this.current.init();
    }
  }

  public Canvas canvas() {
    return this.current;
  }

  public void close() {
    glfwSetWindowShouldClose(this.gameID, true);
  }

  public static Game get() {
    return game;
  }
}
