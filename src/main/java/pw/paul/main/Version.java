/*
 * Copyright by Paul 15|4|2020
 */

package pw.paul.main;

import lombok.*;

@RequiredArgsConstructor
public class Version {

  @Getter
  private final int major;

  @Getter
  private int minor, patch;

  @Getter
  private String stage = "";

  public static final char ALPHA = 'α', BETA = 'β';

  public Version minor(int minor) {
    this.minor = minor;
    return this;
  }

  public Version patch(int patch) {
    this.patch = patch;
    return this;
  }

  public Version stage(String stage) {
    this.stage = stage;
    return this;
  }

  @Override
  public String toString() {
    return this.major + "." + this.minor + "." + this.patch + (this.stage.isEmpty() ? ""
      : "-" + this.stage);
  }
}
