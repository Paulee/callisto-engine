/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.main;

import pw.paul.main.game.Game;
import pw.paul.service.ServiceHandler;

import java.util.Random;

import static org.lwjgl.glfw.GLFW.*;

public class Callisto {

  private static final Random random = new Random();

  public static final long SECOND = 1000000000L;

  public static final double TAU = 2 * Math.PI;

  public static boolean isMouseButtonHold(int button) {
    return glfwGetMouseButton(Game.get().getGameID(), button) == GLFW_PRESS;
  }

  public static boolean isKeyHold(int key) {
    return glfwGetKey(Game.get().getGameID(), key) == GLFW_PRESS;
  }

  public static boolean isKeyHold(String key) {
    if (!ServiceHandler.getService().inputService().getKeys().containsKey(key)) {
      return false;
    }

    return ServiceHandler.getService().inputService().getKeys().get(key).stream()
      .anyMatch(Callisto::isKeyHold);
  }

  public static void hideCursor() {
    glfwSetInputMode(Game.get().getGameID(), GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
  }

  public static void showCursor() {
    glfwSetInputMode(Game.get().getGameID(), GLFW_CURSOR, GLFW_CURSOR_NORMAL);
  }

  public static float randf() {
    return random.nextFloat();
  }

  public static float randf(int offset) {
    return randf() * offset;
  }

  public static float randf(int min, int max) {
    return randf(max - min) + min;
  }

  public static String capitalize(String strIn) {
    return strIn.substring(0, 1).toUpperCase() + strIn.substring(1).toLowerCase();
  }

}
