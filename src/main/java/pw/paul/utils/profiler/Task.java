/*
 * Copyright by Paul 12|5|2020
 */

package pw.paul.utils.profiler;

import lombok.*;

@RequiredArgsConstructor
public class Task {

  @Getter
  private final String displayName;

  @Getter
  @Setter
  private long startTime, endTime;

}
