/*
 * Copyright by Paul 12|5|2020
 */

package pw.paul.utils.profiler;

import lombok.Getter;
import pw.paul.logging.Logger;

import java.util.*;

public class Profiler {

  private static final Profiler profiler = new Profiler();

  @Getter
  private final Map<String, Task> tasks = new LinkedHashMap<>();

  private Profiler() {}

  public void start(String name) {
    this.start(name, name);
  }

  public void start(String name, String taskName) {
    if (!this.tasks.containsKey(name)) {
      this.tasks.put(name, new Task(taskName));
    }

    this.tasks.get(name).setStartTime(System.nanoTime());
  }

  public void end(String name) {
    if (!this.tasks.containsKey(name)) {
      Logger.debug("No Task found(" + name + ")");
    }


    this.tasks.get(name).setEndTime(System.nanoTime());
  }

  public float time(String name) {
    if (!this.tasks.containsKey(name)) {
      Logger.debug("No Task found(" + name + ")");
      return 0;
    }

    return (this.tasks.get(name).getEndTime() - this.tasks.get(name)
      .getStartTime()) / 1000000f;
  }

  public Task task(String name) {
    return this.tasks.get(name);
  }

  public static Profiler get() {
    return profiler;
  }

}
