/*
 * Copyright by Paul 18|5|2020
 */

package pw.paul.utils.hardware;

public enum OS {

  WINDOWS,
  MAC,
  LINUX,
  UNKNOWN

}
