/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.utils.hardware;

import lombok.Getter;
import org.lwjgl.PointerBuffer;
import org.lwjgl.glfw.GLFWVidMode;
import pw.paul.logging.Logger;


import java.nio.file.Path;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static pw.paul.utils.hardware.OS.*;

public class Hardware {

  private static final Hardware hardware = new Hardware();

  @Getter
  private final OS operatingSystem;

  @Getter
  private final long[] monitors;

  private Hardware() {
    this.operatingSystem = this.checkOS();

    PointerBuffer monitors = glfwGetMonitors();

    assert monitors != null : "No Monitor?!";
    this.monitors = new long[monitors.capacity()];
    for ( int i = 0; i < monitors.capacity(); i++ ) {
      this.monitors[i] = monitors.get(i);
    }

  }

  public boolean isPrimary(long monitorAddress) {
    return glfwGetPrimaryMonitor() == monitorAddress;
  }

  public GLFWVidMode getMode(long monitorAddress) {
    return glfwGetVideoMode(monitorAddress);
  }

  private OS checkOS() {
    String os = System.getProperty("os.name");

    if (os.startsWith("Windows")) {
      return WINDOWS;
    } else if (os.startsWith("Linux")) {
      return LINUX;
    } else if (os.startsWith("Mac")) {
      return MAC;
    }

    return UNKNOWN;
  }

  public Path systemPath(OS index) {
    return switch (index) {
      case WINDOWS -> Path.of(System.getenv("LOCALAPPDATA"));
      case MAC -> Path
        .of(System.getProperty("user.home") + "/Library/Application Support");
      default -> Path.of(System.getProperty("user.home"));
    };
  }

  public static boolean is64Bit() {
    String model = System.getProperty("sun.arch.data.model"), arch = System
      .getProperty("os.arch");
    return model.contains("64") || arch.contains("64");
  }

  public static boolean isJava14() {
    return System.getProperty("java.specification.version").equals("14");
  }

  public void specs() {
    System.out.println("------SYSTEM------");
    Logger.debug("Monitor: " + this.monitors.length);
    Logger.debug("Renderer: " + glGetString(GL_RENDERER));
    System.out.println();
    Logger
      .debug(System.getProperty("os.name") + " | " + System.getProperty("os.version"));
    Logger.debug("Java 64 Bit: " + is64Bit());
    Logger.debug("Java 14: " + isJava14());
    System.out.println("--------------------\n");
  }

  public static Hardware get() {
    return hardware;
  }

}
