/*
 * Copyright by Paul 6|9|2020
 */

package pw.paul.utils.archive;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import pw.paul.logging.Logger;

public final class Archive {

  private final List<Path> pathList = new ArrayList<>();

  private final String archiveName;

  private Packager packager;

  private Path input, output;

  private Archive(String archiveName) {
    this.archiveName = archiveName;
  }

  public static Archive create(String archiveName) {
    return new Archive(archiveName);
  }

  public static Archive create(String archiveName, Packager packager) {
    return new Archive(archiveName).packager(packager);
  }

  public static Archive create(
    String archiveName, Packager packager, Path input,
    Path output
  ) {
    return new Archive(archiveName).packager(packager).withInput(input)
      .withOutput(output);
  }

  public Archive withInput(Path input) {
    this.input = input;
    return this;
  }

  public Archive withOutput(Path output) {
    this.output = output;
    return this;
  }

  public Archive add(Path path) {
    if (!this.pathList.contains(path)) {
      this.pathList.add(path);
    }
    return this;
  }

  public Archive add(Path[] pathArray) {
    for ( Path path : pathArray ) {
      this.add(path);
    }
    return this;
  }

  public Archive add(List<Path> pathList) {
    pathList.forEach(this::add);
    return this;
  }

  public Archive packager(Packager packager) {
    this.packager = packager;
    return this;
  }

  public void remove(Path path) {
    //TODO remove from Archive!
    throw new UnsupportedOperationException();
  }

  public Archive remove(Path[] pathArray) {
    for ( Path path : pathArray ) {
      this.remove(path);
    }
    return this;
  }

  public Archive remove(List<Path> pathList) {
    pathList.forEach(this::remove);
    return this;
  }

  public void pack() {
    if (this.packager == null) {
      throw new IllegalStateException("No Packager was defined!");
    }

    if (this.packager.compress(this.pathList, this.output == null ?
      Path.of(this.archiveName.concat(this.packager.getSuffix())) :
      this.output.resolve(this.archiveName.concat(this.packager.getSuffix())))) {
      Logger.debug("Successfully packed Archive!");
    }
  }

  public void unpack() {
    if (this.packager == null) {
      throw new IllegalStateException("No Packager was defined!");
    }

    if (this.packager.decompress(
      this.input == null ?
        Path.of(this.archiveName.concat(this.packager.getSuffix())) :
        this.input.resolve(this.archiveName.concat(this.packager.getSuffix())),
      this.output == null ?
        Path.of(this.archiveName) :
        this.output.resolve(this.archiveName)
    )) {
      Logger.debug("Successfully unpacked Archive!");
    }
  }


  public String getArchiveName() {
    return this.archiveName;
  }

  public Packager getPackager() {
    return this.packager;
  }
}
