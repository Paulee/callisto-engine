/*
 * Copyright by Paul 6|9|2020
 */

package pw.paul.utils.archive.gzip;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import pw.paul.utils.archive.Packager;

public final class GZipPackager implements Packager {

  /**
   * @deprecated Use {@link pw.paul.utils.archive.ArchiveService#GZIP} instead.
   */
  @Deprecated()
  public GZipPackager() {}

  @Override
  public boolean compress(
    List<Path> input, Path output
  ) {
    if (input.size() != 1) {
      throw new IllegalStateException("Unable to create .gz File with more or less than" +
        " 1 File");
    }
    String oldSuffix = input.get(0).getFileName().toString()
      .substring(output.getFileName().toString().lastIndexOf(".") - 1);
    try (
      GZIPOutputStream outputStream =
        new GZIPOutputStream(new FileOutputStream(
          Path
            .of(output.toString().replace(this.getSuffix(), oldSuffix + this.getSuffix()))
            .toFile()));
      FileInputStream inputStream = new FileInputStream(input.get(0).toFile())
    ) {

      outputStream.write(inputStream.readAllBytes());
      return true;
    } catch (IOException ignored) {
    }
    return false;
  }

  @Override
  public boolean decompress(Path input, Path output) {
    AtomicBoolean state = new AtomicBoolean(false);
    try {
      String originalName = input.getFileName().toString()
        .substring(0, input.getFileName().toString().indexOf("."));
      Files.list(input.getParent())
        .filter(path -> path.toString().endsWith(this.getSuffix())).forEach(path -> {
        String name = path.getFileName().toString().substring(
          0,
          path.getFileName().toString().indexOf(".")
        );
        if (name.equals(originalName) && !state.get()) {
          String oldSuffix = path.getFileName().toString().substring(
            path.getFileName().toString().indexOf("."),
            path.getFileName().toString().lastIndexOf(".")
          );
          try (
            GZIPInputStream inputStream =
              new GZIPInputStream(new FileInputStream(path.toFile()));
            FileOutputStream outputStream =
              new FileOutputStream(Path
                .of((output.getParent() == null ? "" : output.getParent()).toString()
                  .concat(originalName).concat(oldSuffix))
                .toFile())
          ) {

            outputStream.write(inputStream.readAllBytes());
            state.set(true);
          } catch (IOException ignored) {
          }
        }
      });
    } catch (IOException ignored) {
    }
    return state.get();
  }

  @Override
  public String getSuffix() {
    return ".gz";
  }
}
