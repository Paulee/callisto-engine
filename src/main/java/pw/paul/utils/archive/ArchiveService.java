/*
 * Copyright by Paul 6|9|2020
 */

package pw.paul.utils.archive;


import java.nio.file.Path;
import java.util.Optional;

import pw.paul.service.Service;
import pw.paul.utils.archive.gzip.GZipPackager;
import pw.paul.utils.archive.zip.ZipPackager;

public final class ArchiveService implements Service {

  /*
    TODO maybe
      .tar
      .7zip
      Archive merge
   */
  public static final ZipPackager ZIP = new ZipPackager();
  public static final GZipPackager GZIP = new GZipPackager();

  public Optional<Packager> getFromSuffix(Path path) {
    if(path.endsWith(".zip")){
      return Optional.of(ZIP);
    }else if(path.endsWith(".gz")){
      return Optional.of(GZIP);
    }
    return Optional.empty();
  }
}
