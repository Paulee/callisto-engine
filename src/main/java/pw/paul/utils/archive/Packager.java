/*
 * Copyright by Paul 6|9|2020
 */

package pw.paul.utils.archive;

import java.nio.file.Path;
import java.util.List;

public interface Packager {

  boolean compress(List<Path> input, Path output);

  boolean decompress(Path input, Path output);

  String getSuffix();
}
