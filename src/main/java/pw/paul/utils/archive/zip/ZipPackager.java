/*
 * Copyright by Paul 6|9|2020
 */

package pw.paul.utils.archive.zip;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import pw.paul.logging.Logger;
import pw.paul.utils.archive.Packager;

public final class ZipPackager implements Packager {

  private boolean succeeded = true;

  /**
   * @deprecated Use {@link pw.paul.utils.archive.ArchiveService#ZIP} instead.
   */
  @Deprecated()
  public ZipPackager() {}

  @Override
  public boolean compress(List<Path> input, Path output) {
    URI uri = URI.create("jar:" + output.toUri());

    System.out.println(uri);
    input.stream().filter(Files::exists).forEach(path -> this.process(path, uri));

    return this.succeeded;
  }

  private void process(Path input, URI uri) {
    if (Files.isDirectory(input)) {
      try {
        Files.walkFileTree(input, new SimpleFileVisitor<>() {
          @Override
          public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) {

            if (attributes.isSymbolicLink()) {
              return FileVisitResult.CONTINUE;
            }

            try (
              FileSystem fileSystem = FileSystems
                .newFileSystem(uri, Collections.singletonMap("create", "true"))
            ) {

              Path zipPath = fileSystem.getPath(input.relativize(file).toString());

              if (zipPath.getParent() != null) {
                Files.createDirectories(zipPath.getParent());
              }

              Logger.debug("packing: " + file.toString());

              Files.copy(file, zipPath, StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES, LinkOption.NOFOLLOW_LINKS
              );

            } catch (IOException e) {
              e.printStackTrace();
            }
            return FileVisitResult.CONTINUE;
          }

          @Override
          public FileVisitResult visitFileFailed(Path file, IOException exc) {
            Logger.debug("Can't zip!(" + file.toString() + ")");
            return FileVisitResult.CONTINUE;
          }
        });
      } catch (IOException ignored) {
        Logger.debug("Something went wrong while zipping!(" + input.toString() + ")");
        this.succeeded = false;
      }
    } else {
      try (
        FileSystem fileSystem = FileSystems.newFileSystem(
          uri,
          Collections.singletonMap("create", "true")
        )
      ) {
        Path zipPath = fileSystem.getPath(input.getFileName().toString());

        Logger.debug("packing: " + input.getFileName().toString());

        Files.copy(input, zipPath, StandardCopyOption.REPLACE_EXISTING);
      } catch (IOException ignored) {
        Logger.debug("Something went wrong while zipping!(" + input.toString() + ")");
        this.succeeded = false;
      }
    }
  }

  @Override
  public boolean decompress(Path input, Path output) {
    try (
      ZipInputStream archiveStream = new ZipInputStream(
        new FileInputStream(input.toFile()))
    ) {
      ZipEntry entry = archiveStream.getNextEntry();

      while (entry != null) {

        boolean badStructure = false;

        Path newPath = output.resolve(entry.getName());

        Path normalizePath = newPath.normalize();
        if (!normalizePath.startsWith(output)) {
          Logger.debug("Skipping bad Archive File!(" + entry.getName() + ")");
          badStructure = true;
        }

        if (!badStructure) {
          Logger.debug("Processing: " + entry.getName());

          if (entry.isDirectory()) {
            Files.createDirectories(newPath);
          } else {
            if (newPath.getParent() != null) {
              if (Files.notExists(newPath.getParent())) {
                Files.createDirectories(newPath.getParent());
              }
            }

            Files.copy(archiveStream, newPath,
              StandardCopyOption.REPLACE_EXISTING
            );

          }
        }
        entry = archiveStream.getNextEntry();
      }
      archiveStream.closeEntry();
      return true;
    } catch (IOException ignored) {
      Logger.debug("Something went wrong while unzipping!(" + output.toString() + ")");
    }
    return false;
  }

  @Override
  public String getSuffix() {
    return ".zip";
  }
}
