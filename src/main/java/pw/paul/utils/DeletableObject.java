/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.utils;

import pw.paul.render.shader.Shader;
import pw.paul.render.texture.Texture2D;
import pw.paul.service.ServiceHandler;
import pw.paul.sound.Sound;

import java.util.*;

public class DeletableObject {

  private static final List<DeletableObject> objects = new ArrayList<>();

  protected String name;

  public DeletableObject(String name) {
    this.name = name;
  }

  protected void add(DeletableObject test) {
    objects.add(test);
  }

  public void add() {
    this.add(this);
  }

  public static void remove(DeletableObject obj) {
    objects.remove(obj);
  }

  protected void delete() {}

  public static <T extends DeletableObject> T get(String name, Class<T> clazz) {
    ServiceHandler.getService().assetsService().loadAsset(
      name, (byte) (clazz == Texture2D.class ? 0
        : clazz == Shader.class ? 1 : clazz == Sound.class ? 2 : 3));
    return clazz.cast(objects.stream()
      .filter(obj -> obj.name.equals(name) && clazz.equals(obj.getClass())).findFirst()
      .orElse(null));
  }

  public static void deleteAll() {
    objects.forEach(DeletableObject::delete);
  }

}
