/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.utils.buffer;

import org.lwjgl.BufferUtils;
import pw.paul.logging.Logger;
import pw.paul.service.ServiceHandler;

import java.io.*;
import java.nio.*;
import java.util.Objects;

import static org.lwjgl.stb.STBImage.stbi_load_from_memory;

public class Buffers {

  public static final ByteBuffer COLOR_BUFFER =
    ByteBuffer.allocateDirect(Integer.BYTES).putInt(-1).flip();

  public static FloatBuffer floatBuffer(float[] data) {
    return BufferUtils.createFloatBuffer(data.length).put(data).flip();
  }

  public static ByteBuffer parse(String path) {
    return stbi_load_from_memory(
      Objects.requireNonNull(ioToBuffer(path)), BufferUtils.createIntBuffer(1),
      BufferUtils.createIntBuffer(1), BufferUtils.createIntBuffer(1), 4
    );
  }

  public static ByteBuffer ioToBuffer(String resource) {
    if (ServiceHandler.getService().assetsService()
      .createStream(resource) == null && resource
      .endsWith(".png")) {
      Logger.debug(resource + " does not exist!");
      return COLOR_BUFFER;
    }

    try (
      InputStream is = ServiceHandler.getService().assetsService().createStream(resource)
    ) {
      assert is != null;
      byte[] data = is.readAllBytes();

      return BufferUtils.createByteBuffer(data.length).put(data).flip();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return null;
  }

}
