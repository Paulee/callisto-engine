/*
 * Copyright by Paul 23|7|2020
 */

package pw.paul.utils.tray;

import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import pw.paul.logging.Logger;

public final class TrayAdapter {

  public static boolean disableDefaultTray;

  private final String toolTip;

  private static final SystemTray SYSTEM_TRAY = SystemTray.getSystemTray();
  private final PopupMenu menu = new PopupMenu();
  private TrayIcon trayIcon;

  public TrayAdapter(String toolTip) {
    this.toolTip = toolTip;
  }

  public TrayAdapter build(InputStream iconStream) {
    try {
      this.trayIcon = new TrayIcon(
        Toolkit.getDefaultToolkit().createImage(iconStream.readAllBytes()),
        this.toolTip, this.menu
      );
      this.trayIcon.setImageAutoSize(true);

      iconStream.close();
    } catch (IOException ignored) {
    }
    return this;
  }

  public TrayAdapter addItem(String label, ActionListener actionListener) {
    this.checkInitialization();

    MenuItem item = new MenuItem(label);
    item.addActionListener(actionListener);
    this.menu.add(item);

    return this;
  }

  public TrayAdapter seperator() {
    this.checkInitialization();

    this.menu.addSeparator();
    return this;
  }

  public void finish() {
    this.checkInitialization();

    try {
      SYSTEM_TRAY.add(this.trayIcon);
    } catch (AWTException ignored) {
    }
  }

  private void checkInitialization() {
    if (this.trayIcon == null) {
      Logger.exception("Build method was never called!");
    }
  }

  public static boolean isTraySupported() {
    return SystemTray.isSupported();
  }
}
