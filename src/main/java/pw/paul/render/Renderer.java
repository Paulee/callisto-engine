/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.render;

import pw.paul.enviroment.Environment;
import pw.paul.ecs.entity.Entity;
import pw.paul.main.game.Game;
import pw.paul.math.Matrix4f;
import pw.paul.math.vec.Vec4f;
import pw.paul.render.font.FontRenderer;
import pw.paul.render.model.Mesh;
import pw.paul.render.shader.Shader;
import pw.paul.render.texture.Texture2D;
import pw.paul.service.ServiceHandler;
import pw.paul.service.event.EventTarget;
import pw.paul.utils.DeletableObject;

import pw.paul.service.event.impl.*;
import pw.paul.utils.profiler.Profiler;

import java.awt.*;
import java.util.*;
import java.util.List;

import pw.paul.render.texture.sprite.*;

import java.util.concurrent.atomic.AtomicInteger;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static pw.paul.render.CallistoRender.*;

public class Renderer {

  private final Mesh uiModel, gameModel;

  private final Shader uiShader;

  private FrameBufferObject fbo;

  private boolean profiler, hitboxes;

  private final FontRenderer fontRenderer;

  private final Map<Sprite, List<Entity>> sprites = new LinkedHashMap<>();

  public Renderer(int width, int height) {
    float[] meshData = new float[]{
      0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1
    }, meshUV = new float[]{
      0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0
    };

    this.uiModel = new Mesh("uiModel", meshData);
    this.gameModel = new Mesh("gameModel", meshData, meshUV);

    this.uiShader = Shader.UIShader;

    this.updateBuffer(width, height);

    this.fontRenderer = new FontRenderer(new Font("Arial", Font.PLAIN, 20));

    ServiceHandler.getService().eventService().register(this);
  }

  private void add(Entity entity) {
    if (entity.hasSprite()) {
      this.sprites.computeIfAbsent(entity.getSprite(), k -> new ArrayList<>())
        .add(entity);
    }

  }

  private void pre(int width, int height) {
    glDisable(GL_DEPTH_TEST);

    this.uiShader.bind();
    this.uiShader.setUniformVec("matColor", new Vec4f(1, 1, 1, 1));

    this.uiShader.setMatrix("projection", Matrix4f.ortho(0, width, height, 0, -1, 1));

    this.uiModel.bind();
  }

  private void post() {
    this.uiModel.unbind();
    this.uiShader.unbind();
  }

  private void renderToFBO() {
    this.fbo.bind();
    glClearColor(156 / 255f, 252 / 255f, 118 / 255f, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    Environment.get().entities().forEach(this::add);

    this.gameModel.bind();
    this.sprites.keySet().forEach(sprite -> {
      sprite.bind();
      sprite.getShader().setMatrix("projection", Matrix4f
        .ortho(-(this.fbo.getWidth() * .5f), this.fbo.getWidth() * .5f,
          this.fbo.getHeight() * .5f, -(this.fbo.getHeight() * .5f), -1, 1
        ));
      sprite.getShader().setUniformVec("matColor",
        new Vec4f((sprite.getColor() >> 16 & 0xFF) / 255f,
          (sprite.getColor() >> 8 & 0xFF) / 255f, (sprite.getColor() & 0xFF) / 255f,
          (sprite.getColor() >> 24 & 0xFF) / 255f
        )
      );
      this.sprites.get(sprite).forEach(entity -> {
        sprite.set(entity);
        this.gameModel.draw();
      });
      sprite.unbind();
    });

    this.gameModel.unbind();
    this.sprites.clear();

    this.fbo.unbind();
  }

  private void updateBuffer(int width, int height) {
    if (width != 0 && height != 0) {
      this.fbo = new FrameBufferObject(width, height);
    }
  }

  @EventTarget
  public void onRender(RenderEvent e) {
    Profiler.get().start("game-render", "Render(Game)");
    this.renderToFBO();
    this.pre(e.getWidth(), e.getHeight());
    CldrawTexture(this.fbo.getTexture(), new float[]{0, 0, e.getWidth(), e.getHeight()});
    Profiler.get().end("game-render");

    Profiler.get().start("ui-render", "Render(GUI)");
    ServiceHandler.getService().eventService()
      .fire(new RenderUIEvent(e.getWidth(), e.getHeight()));
    Profiler.get().end("ui-render");

    this.fontRenderer.drawStringWithShadow("FPS: " + Game.get().getFPS(), 0, 0, -1);

    if (this.hitboxes) {
      Environment.get().entities().forEach(entity -> CldrawRect(new float[]{
        e.getWidth() * .5f + entity.getAabb().getMinX(),
        e.getHeight() * .5f - entity.getAabb().getMinY() - entity.getAabb().getMaxY(),
        entity.getAabb().getMaxX(), entity.getAabb().getMaxY()
      }, entity.isPassable() ? 0x90f90e59 : 0x900e8cf9));

      CldrawRect(new float[]{10, e.getHeight() - 30, 20, 20}, 0xFFf90e59);
      this.fontRenderer
        .drawStringWithShadow("Passible Entity", 35, e.getHeight() - 31, -1);

      CldrawRect(new float[]{10, e.getHeight() - 60, 20, 20}, 0xFF0e8cf9);
      this.fontRenderer
        .drawStringWithShadow("Not passible Entity", 35, e.getHeight() - 61, -1);
    }

    if (this.profiler) {
      CldrawRect(new float[]{20, 20, 320, 28}, 0xBB000000);
      CldrawRect(new float[]{
        20, 20, 320,
        40 + (Profiler.get().getTasks().values().size() * 24) + (Profiler.get().getTasks()
          .values().size() * 7)
      }, 0x90000000);
      this.fontRenderer.drawString("Profiler:", 24, 24, -1);
      this.fontRenderer.drawString(String.valueOf(Game.get().getFPS()),
        336 - this.fontRenderer.getWidth(String.valueOf(Game.get().getFPS())), 24, -1
      );
      AtomicInteger counter = new AtomicInteger(1);
      AtomicInteger linePadding = new AtomicInteger(0);
      Profiler.get().getTasks().keySet().forEach(task -> this.fontRenderer
        .drawStringWithShadow(
          Profiler.get().task(task).getDisplayName() + ": " + Profiler.get()
            .time(task) + "ms", 24,
          35 + this.fontRenderer.getHeight() * counter.getAndIncrement() + linePadding
            .getAndAdd(7), -1
        ));

      CldrawRect(new float[]{e.getWidth() - 280, 20, 260, 28}, 0xBB000000);
      CldrawRect(new float[]{
        e.getWidth() - 280, 20, 260, 40 + (Environment.get().entities().size() * 24)
      }, 0x90000000);
      this.fontRenderer.drawString("Entities:", e.getWidth() - 276, 24, -1);
      this.fontRenderer.drawString(String.valueOf(Environment.get().entities().size()),
        e.getWidth() - 24 - this.fontRenderer
          .getWidth(String.valueOf(Environment.get().entities().size())), 24, -1
      );
      AtomicInteger counter2 = new AtomicInteger(1);
      Environment.get().entities().forEach(entity -> this.fontRenderer
        .drawStringWithShadow(entity.getClass().getSimpleName(), e.getWidth() - 276,
          35 + this.fontRenderer.getHeight() * counter2.getAndIncrement(), -1
        ));

      this.fontRenderer.drawStringWithShadow("made with Callisto by Paul",
        e.getWidth() - this.fontRenderer.getWidth("made with Callisto by Paul") - 36,
        e.getHeight() - this.fontRenderer.getHeight() - 4, -1
      );
      CldrawTexture(
        DeletableObject.get("icon", Texture2D.class),
        new float[]{e.getWidth() - 34, e.getHeight() - 34, 32, 32}
      );
    }

    this.post();
  }

  @EventTarget
  private void onResize(WindowResizeEvent e) {
    this.updateBuffer(e.getWidth(), e.getHeight());
  }

  @EventTarget
  public void onKey(KeyReleaseEvent e) {
    if (e.getKey() == GLFW_KEY_END) {
      this.profiler = !this.profiler;
    }

    if (e.getKey() == GLFW_KEY_H) {
      this.hitboxes = !this.hitboxes;
    }

    glfwSetWindowShouldClose(Game.get().getGameID(), e.getKey() == GLFW_KEY_DELETE);
  }
}
