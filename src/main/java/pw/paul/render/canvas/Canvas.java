/*
 * Copyright by Paul 8|6|2020
 */

package pw.paul.render.canvas;

import lombok.Getter;
import pw.paul.render.CallistoRender;
import pw.paul.render.canvas.element.IElement;
import pw.paul.service.event.impl.RenderUIEvent;

import java.util.ArrayList;
import java.util.List;

public abstract class Canvas extends CallistoRender {

  @Getter
  private final Canvas parentCanvas;

  @Getter
  private final List<IElement> elements = new ArrayList<>();

  public Canvas() {
    this(null);
  }

  public Canvas(Canvas parentCanvas) {
    this.parentCanvas = parentCanvas;
  }

  public void init() {}

  public abstract void render(RenderUIEvent e);

  public void add(IElement element) {
    this.elements.add(element);
  }

}
