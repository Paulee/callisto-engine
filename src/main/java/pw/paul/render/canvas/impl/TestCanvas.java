/*
 * Copyright by Paul 8|6|2020
 */

package pw.paul.render.canvas.impl;

import pw.paul.enviroment.Environment;
import pw.paul.render.canvas.Canvas;
import pw.paul.render.canvas.element.impl.Button;
import pw.paul.render.font.FontRenderer;
import pw.paul.render.texture.Texture2D;
import pw.paul.service.event.EventTarget;
import pw.paul.service.event.impl.RenderUIEvent;
import pw.paul.utils.DeletableObject;

import java.awt.*;


public class TestCanvas extends Canvas {

    private Button button;

    @Override
    public void init(){
        this.add(this.button = new Button("Play", 600 - 90, 600, 180, 40, new FontRenderer(new Font("Arial", Font.PLAIN, 24))));

        this.button.action((btn, clicked) -> {
            if(btn == 0)
                Environment.get().loadEnviroment();
        });

    }

    @EventTarget
    public void render(RenderUIEvent e){
        CldrawTexture(DeletableObject.get("title", Texture2D.class), new float[]{0, 0, e.getWidth(), e.getHeight()});
        this.button.draw((x, y, w, h, hovered) -> {
            CldrawRect(new float[]{x, y, w, h}, hovered ? 0xFFfc3a3d : -1);
            this.button.textColor(hovered ? -1 : 0xFFfc3a3d);
        });

    }
}
