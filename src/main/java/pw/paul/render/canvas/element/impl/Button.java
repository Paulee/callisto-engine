/*
 * Copyright by Paul 15|6|2020
 */

package pw.paul.render.canvas.element.impl;

import lombok.*;
import pw.paul.render.canvas.element.IElement;
import pw.paul.render.canvas.element.callbacks.*;
import pw.paul.render.font.FontRenderer;
import pw.paul.service.ServiceHandler;

@RequiredArgsConstructor
public class Button implements IElement {

  private final String text;

  private final float x, y, width, height;

  private final FontRenderer renderer;

  private IClickCallback clickCallback;

  private int color = -1;

  public void textColor(int color) {
    this.color = color;
  }

  public void action(IClickCallback clickCallback) {
    this.clickCallback = clickCallback;
  }

  public void draw(IDrawCallback callback) {
    callback.draw(this.x, this.y, this.width, this.height, this.hovered());

    this.renderer.drawCentredStringWithShadow(this.text, this.x + this.width / 2,
      this.y + this.height / 2 - this.renderer.getHeight() / 2, this.color
    );
  }

  public void click(int button) {
    this.clickCallback.click(button, this);
  }

  public boolean hovered() {
    return this.x <= ServiceHandler.getService().inputService().mouse
      .getX() && ServiceHandler.getService().inputService().mouse
      .getX() <= (this.x + this.width) && this.y <= ServiceHandler.getService()
      .inputService().mouse.getY() && ServiceHandler.getService().inputService().mouse
      .getY() <= this.y + this.height;
  }

}
