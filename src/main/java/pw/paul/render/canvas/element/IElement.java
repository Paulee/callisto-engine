/*
 * Copyright by Paul 15|6|2020
 */

package pw.paul.render.canvas.element;

import pw.paul.render.canvas.element.callbacks.IDrawCallback;

public interface IElement {

  void draw(IDrawCallback callback);

  void click(int button);

  boolean hovered();

}
