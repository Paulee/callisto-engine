/*
 * Copyright by Paul 15|6|2020
 */

package pw.paul.render.canvas.element.callbacks;

public interface IDrawCallback {

  void draw(float x, float y, float widthIn, float heightIn, boolean hovered);

}
