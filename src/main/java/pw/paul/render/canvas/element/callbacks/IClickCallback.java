/*
 * Copyright by Paul 15|6|2020
 */

package pw.paul.render.canvas.element.callbacks;

import pw.paul.render.canvas.element.IElement;

public interface IClickCallback {

  void click(int button, IElement element);

}
