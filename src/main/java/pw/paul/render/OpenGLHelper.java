/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.render;

import org.lwjgl.Version;
import pw.paul.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20C.GL_SHADING_LANGUAGE_VERSION;

public final class OpenGLHelper {

  public static final String LWJGL_VERSION = Version.getVersion(),
    GLFW_Version = glfwGetVersionString(),
    OPENGL_Version = glGetString(GL_VERSION),
    GLSL_Version = glGetString(GL_SHADING_LANGUAGE_VERSION),
    VENDOR = glGetString(GL_VENDOR),
    RENDERER = glGetString(GL_RENDERER);

  private OpenGLHelper() {}

  public static void prints() {
    System.out.println("---------GL---------");
    Logger.debug("lwjgl " + LWJGL_VERSION);
    Logger.debug("GLFW: " + GLFW_Version);
    Logger.debug("OpenGL: " + OPENGL_Version);
    Logger.debug("GLSL: " + GLSL_Version);
    Logger.debug("Vendor: " + VENDOR + " | Renderer " + RENDERER);
    System.out.println("--------------------\n");
  }

  public static void checkErrors() {
    int glError = glGetError(), glfwError = glfwGetError(null);

    if (glError != GL_NO_ERROR) {
      Logger.debug("Error(OpenGL): " + glError + " | " + getString(glError));
    }

    if (glfwError != GLFW_NO_ERROR) {
      Logger.debug("Error(GLFW): " + glfwError + " | " + getString(glfwError));
    }
  }

  private static String getString(int code) {
    return switch (code) {
      case GL_INVALID_ENUM, GLFW_INVALID_ENUM -> "Invalid Enum";
      case GL_INVALID_VALUE, GLFW_INVALID_VALUE -> "Invalid Value";
      case GL_INVALID_OPERATION -> "Invalid Operation";
      case GL_STACK_OVERFLOW -> "Stack Overflow";
      case GL_STACK_UNDERFLOW -> "Stack Underflow";
      case GL_OUT_OF_MEMORY, GLFW_OUT_OF_MEMORY -> "Out of Memory";
      case 1286 -> "Invalid Framebuffer Operation";
      case 1287 -> "Context lost";
      case 32817 -> "Table too large";
      case GLFW_NOT_INITIALIZED -> "GLFW was'nt initialized";
      case GLFW_NO_CURRENT_CONTEXT -> "No Context";
      case GLFW_API_UNAVAILABLE -> "API unavailable";
      case GLFW_VERSION_UNAVAILABLE -> "Version unavailable";
      case GLFW_PLATFORM_ERROR -> "Platform Error";
      case GLFW_FORMAT_UNAVAILABLE -> "Format unavailable";
      default -> "Unknown Error";
    };
  }
}
