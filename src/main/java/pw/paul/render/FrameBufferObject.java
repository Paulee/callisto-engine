/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.render;

import lombok.Getter;
import org.lwjgl.opengl.GL32;
import pw.paul.render.texture.Texture2D;

import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL30.*;

public class FrameBufferObject {

  private int fboID = -1;

  @Getter
  private final Texture2D texture;

  @Getter
  private final int width, height;

  public FrameBufferObject(int width, int height) {
    if (this.fboID != -1) {
      this.delete();
    }

    this.width = width;
    this.height = height;

    this.fboID = glGenFramebuffers();
    glBindFramebuffer(GL_FRAMEBUFFER, this.fboID);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);

    int textureID = glGenTextures();
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGBA, this.width, this.height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
      (ByteBuffer) null
    );

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    GL32.glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, textureID, 0);

    this.texture = new Texture2D("fbo", textureID, width, height);
  }

  public void bind() {
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, this.fboID);
  }

  public void unbind() {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  }

  private void delete() {
    glDeleteFramebuffers(this.fboID);
  }

}
