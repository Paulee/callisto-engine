/*
 * Copyright by Paul 10|5|2020
 */

package pw.paul.render.font;

import pw.paul.math.vec.Vec4f;
import pw.paul.render.model.Mesh;
import pw.paul.render.shader.Shader;
import pw.paul.service.ServiceHandler;
import pw.paul.utils.DeletableObject;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;

import static org.lwjgl.opengl.GL11.*;

public class FontRenderer {

  private int fontID;

  private BufferedImage fontImage;

  private final Font font;
  private FontMetrics metrics;

  private final Map<Character, GlyphData> charData = new HashMap<>();

  private final Shader shader;

  public FontRenderer(
    String fontName, float size
  ) throws IOException, FontFormatException {
    this(fontName, Font.PLAIN, size);
  }

  public FontRenderer(
    String fontName, int style, float size
  ) throws IOException, FontFormatException {
    this(Font.createFont(
      Font.TRUETYPE_FONT,
      ServiceHandler.getService().assetsService().createStream(fontName + ".ttf")
    ).deriveFont(style).deriveFont(size));
  }

  public FontRenderer(Font font) {
    this.font = font;

    this.generate();
    this.shader = Shader.UIShader;
  }

  public void drawCentredString(String text, float x, float y, int color) {
    this.drawString(text, x - this.getWidth(text) / 2, y, color);
  }

  public void drawCentredStringWithShadow(String text, float x, float y, int color) {
    this.drawString(text, x - this.getWidth(text) * .5f + 1, y + 1,
      new Color(color, true).darker().darker().darker().darker().getRGB()
    );
    this.drawString(text, x - this.getWidth(text) * .5f, y, color);
  }

  public void drawStringWithShadow(String text, float x, float y, int color) {
    this.drawString(text, x + 1, y + 1,
      new Color(color, true).darker().darker().darker().darker().darker().getRGB()
    );
    this.drawString(text, x, y, color);
  }

  public void drawString(String text, float x, float y, int color) {
    float xPos = x;

    glBindTexture(GL_TEXTURE_2D, this.fontID);

    this.shader.setUniformVec(
      "matColor",
      new Vec4f((color >> 16 & 0xFF) / 255f, (color >> 8 & 0xFF) / 255f,
        (color & 0xFF) / 255f, (color >> 24 & 0xFF) / 255f
      )
    );
    for ( char c : text.toCharArray() ) {
      GlyphData glyph = this.charData.get(c);

      this.shader.setUniform2f("screenPos", xPos, y);
      this.shader.setUniform4f("offset", glyph.getX(), glyph.getY(), glyph.getWidth(),
        glyph.getHeight()
      );
      this.shader.setUniform2f("pixelScale", glyph.getScaleX(), glyph.getScaleY());
      DeletableObject.get("uiModel", Mesh.class).draw();

      xPos += glyph.getScaleX();
    }
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  private void generate() {
    GraphicsConfiguration configuration = GraphicsEnvironment
      .getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();
    Graphics2D graphic = configuration
      .createCompatibleImage(1, 1, Transparency.TRANSLUCENT).createGraphics();
    graphic.setFont(this.font);

    this.metrics = graphic.getFontMetrics();
    this.fontImage = graphic.getDeviceConfiguration()
      .createCompatibleImage(1024, 1204, Transparency.TRANSLUCENT);

    this.fontID = glGenTextures();
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, this.fontID);

    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE,
      this.createTexture()
    );

    //TODO: maybe GL_LINEAR
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }

  private ByteBuffer createTexture() {

    Graphics2D graphic = (Graphics2D) this.fontImage.getGraphics();
    graphic.setFont(this.font);
    graphic.setRenderingHint(
      RenderingHints.KEY_TEXT_ANTIALIASING,
      RenderingHints.VALUE_TEXT_ANTIALIAS_ON
    );

    this.renderChars(graphic);

    return this.create();
  }

  private void renderChars(Graphics2D graphic) {
    int x = 0;
    int y = 0;
    float height = this.metrics.getMaxAscent() + this.metrics.getMaxDescent();

    for ( int i = 32; i < 256; i++ ) {
      if (i == 127) {
        continue;
      }

      char c = (char) i;

      float width = this.metrics.charWidth(c);
      float advance = width + 8;

      if (x + advance > 1024) {
        x = 0;
        y += 1;
      }

      this.charData.put(
        c,
        new GlyphData(x / (float) 1024, (y * height) / (float) 1024, width / (float) 1024,
          height / (float) 1024, width, height
        )
      );
      graphic
        .drawString(String.valueOf(c), x, this.metrics.getMaxAscent() + (height * y) - 1);
      x += advance;
    }
  }

  private ByteBuffer create() {
    int width = 1024;
    int height = 1024;

    int[] pixels = new int[width * height];
    this.fontImage.getRGB(0, 0, width, height, pixels, 0, width);
    ByteBuffer buffer = ByteBuffer.allocateDirect(width * height * 4);

    for ( int i = 0; i < pixels.length; i++ ) {
      buffer.put((byte) ((pixels[i] >> 16) & 0xFF));
      buffer.put((byte) ((pixels[i] >> 8) & 0xFF));
      buffer.put((byte) ((pixels[i]) & 0xFF));
      buffer.put((byte) ((pixels[i] >> 24) & 0xFF));
    }
    buffer.flip();

    return buffer;
  }

  public float getWidth(String strIn) {
    return this.metrics.stringWidth(strIn);
  }

  public float getHeight() {
    return this.metrics.getHeight();
  }

}
