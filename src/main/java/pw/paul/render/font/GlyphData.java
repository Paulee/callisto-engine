/*
 * Copyright by Paul 10|5|2020
 */

package pw.paul.render.font;

import lombok.*;

@RequiredArgsConstructor
public class GlyphData {

  @Getter
  private final float x, y, width, height, scaleX, scaleY;

}
