/*
 * Copyright by Paul 16|4|2020
 */

package pw.paul.render.shader;

import org.lwjgl.BufferUtils;
import pw.paul.logging.Logger;
import pw.paul.math.Matrix4f;
import pw.paul.math.vec.Vec4f;
import pw.paul.service.ServiceHandler;
import pw.paul.utils.DeletableObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.FloatBuffer;
import java.util.*;

import static org.lwjgl.opengl.GL20.*;

public class Shader extends DeletableObject {

  private final int program, vertexID, fragmentID;

  private final Map<String, Integer> locations = new HashMap<>();

  //Not created by me | cc (Cash)
  private static final String VERTEX_GAME = "#version 140\n" +
    "\n" +
    "in vec2 position;\n" +
    "in vec2 texCoords;\n" +
    "\n" +
    "uniform vec4 matColor;\n" +
    "uniform mat4 projection;\n" +
    "uniform vec2 pixelScale;\n" +
    "uniform vec4 offset;\n" +
    "uniform mat4 transformMatrix;\n" +
    "uniform vec2 anchor;\n" +
    "uniform vec2 objectScale;\n" +
    "\n" +
    "out vec4 color;\n" +
    "out vec2 uvCoords;\n" +
    "\n" +
    "void main(){\n" +
    "    color = matColor;\n" +
    "\n" +
    "    vec4 wPos = vec4((position - anchor) * (pixelScale * objectScale), 0, 1) * transformMatrix;\n" +
    "\n" +
    "    gl_Position = projection * wPos;\n" +
    "    uvCoords = (texCoords * offset.zw) + offset.xy;\n" +
    "}",

  VERTEX_UI = "#version 140\n" +
    "\n" +
    "in vec2 position;\n" +
    "in vec2 texCoords;\n" +
    "\n" +
    "uniform vec4 matColor;\n" +
    "uniform mat4 projection;\n" +
    "uniform vec2 pixelScale;\n" +
    "uniform vec2 screenPos;\n" +
    "uniform vec4 offset;\n" +
    "\n" +
    "out vec4 color;\n" +
    "out vec2 uvCoords;\n" +
    "\n" +
    "void main(){\n" +
    "    color = matColor;\n" +
    "\n" +
    "    gl_Position = projection * vec4((position * pixelScale) + screenPos, 0, 1);\n" +
    "    uvCoords = (texCoords * offset.zw) + offset.xy;\n" +
    "}",

  FRAGMENT = "#version 140\n" +
    "\n" +
    "uniform sampler2D sampler;\n" +
    "\n" +
    "in vec4 color;\n" +
    "in vec2 uvCoords;\n" +
    "\n" +
    "void main(){\n" +
    "    gl_FragColor = color * texture2D(sampler, uvCoords);\n" +
    "}";

  public static Shader UIShader = new Shader("ui", VERTEX_UI, FRAGMENT);
  public static Shader GameShader = new Shader("game", VERTEX_GAME, FRAGMENT);

  public Shader(String name) {
    this(name, load(name + ".vert"), load(name + ".frag"));
  }

  public Shader(String name, String vertexShader, String fragmentShader) {
    super(name);

    this.program = glCreateProgram();

    this.vertexID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(this.vertexID, vertexShader);
    glCompileShader(this.vertexID);

    if (glGetShaderi(this.vertexID, GL_COMPILE_STATUS) != GL_TRUE) {
      Logger.debug("Unable to compile Shader(Vertex) - " + name);
    }

    this.fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(this.fragmentID, fragmentShader);
    glCompileShader(this.fragmentID);

    if (glGetShaderi(this.fragmentID, GL_COMPILE_STATUS) != GL_TRUE) {
      Logger.debug("Unable to compile Shader(Fragment) - " + name);
    }

    glAttachShader(this.program, this.vertexID);
    glAttachShader(this.program, this.fragmentID);

    glBindAttribLocation(this.program, 0, "vertices");
    glBindAttribLocation(this.program, 1, "uv");

    glLinkProgram(this.program);

    if (glGetProgrami(this.program, GL_LINK_STATUS) != GL_TRUE) {
      Logger.debug("Failed to link Program - " + name);
    }

    glValidateProgram(this.program);

    if (glGetProgrami(this.program, GL_VALIDATE_STATUS) != GL_TRUE) {
      Logger.debug("Failed to validate Program - " + name);
    }

    this.add(this);
  }

  public void bind() {
    glUseProgram(this.program);
  }

  public void unbind() {
    glUseProgram(0);
  }

  public int getUniformLoc(String location) {
    if (this.locations.containsKey(location)) {
      return this.locations.get(location);
    }

    int loc = glGetUniformLocation(this.program, location);
    if (loc == -1) {
      Logger.debug("Unable to find Location: " + location + " | Shader: " + this.name);
    } else {
      this.locations.put(location, loc);
    }

    return loc;
  }

  public void setUniform1f(String location, float value) {
    glUniform1f(this.getUniformLoc(location), value);
  }

  public void setUniform2f(String location, float value1, float value2) {
    glUniform2f(this.getUniformLoc(location), value1, value2);
  }

  public void setUniform3f(String location, float value1, float value2, float value3) {
    glUniform3f(this.getUniformLoc(location), value1, value2, value3);
  }

  public void setUniform4f(
    String location, float value1, float value2, float value3, float value4
  ) {
    glUniform4f(this.getUniformLoc(location), value1, value2, value3, value4);
  }

  public void setUniformVec(String location, Vec4f vec) {
    this.setUniform4f(location, vec.getW(), vec.getX(), vec.getY(), vec.getZ());
  }

  public void setMatrix(String location, Matrix4f matrix) {
    FloatBuffer fBuffer = BufferUtils.createFloatBuffer(16);
    matrix.apply(fBuffer);
    glUniformMatrix4fv(this.getUniformLoc(location), false, fBuffer);
    fBuffer.flip();
  }

  public void delete() {
    glDetachShader(this.program, this.vertexID);
    glDetachShader(this.program, this.fragmentID);
    glDeleteShader(this.vertexID);
    glDeleteShader(this.fragmentID);
    glDeleteProgram(this.program);
  }

  private static String load(String file) {
    StringBuilder sb = new StringBuilder();
    try (
      BufferedReader reader = new BufferedReader(new InputStreamReader(
        ServiceHandler.getService().assetsService().createStream(file)))
    ) {
      reader.lines().forEach(line -> sb.append(line).append("\n"));
    } catch (IOException e) {
      e.printStackTrace();
    }
    return sb.toString();
  }
}
