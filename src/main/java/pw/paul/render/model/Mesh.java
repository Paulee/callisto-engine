/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.render.model;

import pw.paul.utils.DeletableObject;
import pw.paul.utils.buffer.Buffers;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

public class Mesh extends DeletableObject {

  private final int vertexID, vertexAO, textureID;

  public Mesh(String name, float[] meshData) {
    this(name, meshData, meshData);
  }

  public Mesh(String name, float[] vertices, float[] textureCoords) {
    super(name);

    this.vertexAO = glGenVertexArrays();
    glBindVertexArray(this.vertexAO);

    this.vertexID = glGenBuffers();
    glBindBuffer(GL_ARRAY_BUFFER, this.vertexID);
    glBufferData(GL_ARRAY_BUFFER, Buffers.floatBuffer(vertices), GL_STATIC_DRAW);
    glVertexAttribPointer(0, vertices.length / 3, GL_FLOAT, false, 0, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    this.textureID = glGenBuffers();
    glBindBuffer(GL_ARRAY_BUFFER, this.textureID);
    glBufferData(GL_ARRAY_BUFFER, Buffers.floatBuffer(textureCoords), GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    this.add(this);
  }

  public void bind() {
    glBindVertexArray(this.vertexAO);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, this.vertexID);
    glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, this.textureID);
    glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
  }

  public void draw() {
    glDrawArrays(GL_TRIANGLES, 0, 6);
  }

  public void unbind() {
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindVertexArray(0);
  }

  protected void delete() {
    glDeleteVertexArrays(this.vertexAO);
    glDeleteBuffers(this.vertexID);
    glDeleteBuffers(this.textureID);
  }
}
