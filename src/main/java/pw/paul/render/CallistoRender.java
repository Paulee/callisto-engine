/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.render;

import pw.paul.math.vec.Vec4f;
import pw.paul.render.model.Mesh;
import pw.paul.render.shader.Shader;
import pw.paul.render.texture.Texture2D;
import pw.paul.utils.DeletableObject;

import static org.lwjgl.opengl.GL11.*;

public class CallistoRender {

  public static void CldrawRect(float[] rectangle, int colorFade) {
    CldrawTextureColor(null, rectangle, colorFade);
  }

  public static void CldrawTexture(Texture2D texture, float[] rectangle) {
    CldrawTextureColor(texture, rectangle, -1);
  }

  public static void CldrawTextureColor(
    Texture2D texture, float[] rectangle, int colorFade
  ) {
    CldrawTextureCC(texture, rectangle, new float[]{0, 0, 1, 1}, colorFade);
  }

  public static void CldrawTextureCoords(
    Texture2D texture, float[] rectangle, float[] textureCoords
  ) {
    CldrawTextureCC(texture, rectangle, textureCoords, -1);
  }

  public static void CldrawTextureCC(
    Texture2D texture, float[] rectangle, float[] textureCoords, int colorFade
  ) {
    glBindTexture(GL_TEXTURE_2D, Texture2D.checkTexture(texture));

    Shader.UIShader
      .setUniform4f("offset", textureCoords[0], textureCoords[1], textureCoords[2],
        textureCoords[3]
      );
    Shader.UIShader.setUniform2f("pixelScale", rectangle[2], rectangle[3]);
    Shader.UIShader.setUniform2f("screenPos", rectangle[0], rectangle[1]);
    Shader.UIShader.setUniformVec("matColor",
      new Vec4f(((colorFade >> 16) & 0xFF) / 255f, ((colorFade >> 8) & 0xFF) / 255f,
        (colorFade & 0xFF) / 255f, ((colorFade >> 24) & 0xFF) / 255f
      )
    );

    DeletableObject.get("uiModel", Mesh.class).draw();
  }

}
