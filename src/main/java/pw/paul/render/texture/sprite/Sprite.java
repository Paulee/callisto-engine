/*
 * Copyright by Paul 7|5|2020
 */

package pw.paul.render.texture.sprite;

import lombok.Getter;
import pw.paul.ecs.entity.Entity;
import pw.paul.render.shader.Shader;
import pw.paul.render.texture.Texture2D;
import pw.paul.utils.DeletableObject;

public class Sprite extends DeletableObject {

  @Getter
  protected Texture2D texture;

  @Getter
  protected int color;

  @Getter
  protected Shader shader;

  protected float[] offset;

  public Sprite(String name, Texture2D textureRef, int color) {
    this(
      name, textureRef, color,
      new float[]{0, 0, textureRef.getWidth(), textureRef.getHeight()}
    );
  }

  public Sprite(String name, Texture2D textureRef, int color, float[] offset) {
    super(name);

    this.texture = textureRef;
    this.color = color;
    this.offset = offset;

    this.shader = Shader.GameShader;
  }

  public void bind() {
    this.shader.bind();
    this.texture.bind();
  }

  public void unbind() {
    this.texture.unbind();
    this.shader.unbind();
  }

  public boolean isAnimated() {
    return this instanceof AnimatedSprite;
  }

  private float[] uv() {
    float width = this.texture.getWidth();
    float height = this.texture.getHeight();

    return new float[]{
      this.offset[0] / width, this.offset[1] / height, this.offset[2] / width,
      this.offset[3] / height
    };
  }

  public void set(Entity entityIn) {
    this.set(entityIn, this.uv());
  }

  public void set(Entity entityIn, float[] uv) {
    this.shader.setUniform2f("anchor", entityIn.getFixPoint().getX(),
      entityIn.getFixPoint().getY()
    );

    if (entityIn.updated()) {
      entityIn.updateMatrix();
    }

    this.shader.setMatrix("transformMatrix", entityIn.getMatrix());
    this.shader.setUniform2f("objectScale", entityIn.getScale().getX(),
      entityIn.getScale().getY()
    );
    this.shader.setUniform2f("pixelScale",
      this.offset[2] / (this.isAnimated() ? (this.offset[2] / ((AnimatedSprite) this)
        .getRightShift()) : 1), this.offset[3]
    );

    this.shader.setUniform4f("offset", uv[0], uv[1], uv[2], uv[3]);
  }

  public void offset(float[] offset) {
    this.offset = offset;
  }
}