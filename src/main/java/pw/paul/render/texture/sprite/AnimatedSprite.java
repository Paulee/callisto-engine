/*
 * Copyright by Paul 22|5|2020
 */

package pw.paul.render.texture.sprite;

import lombok.Getter;
import pw.paul.ecs.entity.Entity;
import pw.paul.main.Callisto;
import pw.paul.render.shader.Shader;

public class AnimatedSprite extends Sprite {

  @Getter
  private final int rightShift;

  private int currentTex;

  private final double fps;

  private double elapsed, last;

  public AnimatedSprite(String name, Sprite spriteRef, int rightShift, float fps) {
    super(name, spriteRef.texture, spriteRef.color, spriteRef.offset);

    this.rightShift = rightShift;
    this.fps = 1 / fps;
    this.last = System.nanoTime() / (double) Callisto.SECOND;

    this.shader = Shader.GameShader;
  }

  public void set(Entity entityIn) {
    float[] uv = new float[4];

    double current = System.nanoTime() / (double) Callisto.SECOND;
    this.elapsed += current - this.last;

    if (this.elapsed >= this.fps) {
      this.elapsed = 0;
      this.currentTex++;
    }

    if (this.currentTex >= this.rightShift) {
      this.currentTex = 0;
    }

    this.last = current;

    uv[0] = (this.rightShift * this.currentTex) / this.offset[2];
    uv[1] = 0;
    uv[2] = 1 / (this.offset[2] / this.rightShift);
    uv[3] = 1;

    this.set(entityIn, uv);
  }
}
