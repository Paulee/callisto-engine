/*
 * Copyright by Paul 22|4|2020
 */

package pw.paul.render.texture;

import lombok.Getter;
import org.lwjgl.BufferUtils;
import pw.paul.utils.DeletableObject;
import pw.paul.utils.buffer.Buffers;

import java.nio.*;
import java.util.Objects;

import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.stb.STBImage.*;

public class Texture2D extends DeletableObject {

  @Getter
  private final int textureID, width, height;

  @Getter
  private static int emptyTexture;

  public Texture2D(String name) {
    super(name);

    IntBuffer wBuffer = BufferUtils.createIntBuffer(1);
    IntBuffer hBuffer = BufferUtils.createIntBuffer(1);

    ByteBuffer img = stbi_load_from_memory(
      Objects.requireNonNull(Buffers.ioToBuffer("assets/textures/" + name + ".png")),
      wBuffer,
      hBuffer, BufferUtils.createIntBuffer(1), 4
    );

    this.textureID = glGenTextures();
    this.width = wBuffer.get();
    this.height = hBuffer.get();

    upload(this.textureID, this.width, this.height, img);

    assert img != null;
    stbi_image_free(img);

    this.add(this);
  }

  public Texture2D(String name, int id, int width, int height) {
    super(name);
    this.textureID = id;
    this.width = width;
    this.height = height;
  }

  public static void allocateEmptyTexture() {
    upload(emptyTexture = glGenTextures(), 1, 1, Buffers.COLOR_BUFFER);
  }

  public static void deleteEmptyTexture() {
    glDeleteTextures(emptyTexture);
  }

  public static int checkTexture(Texture2D texture) {
    return texture == null ? emptyTexture : texture.textureID;
  }

  private static void upload(int textureID, int width, int height, ByteBuffer buffer) {
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE,
      buffer
    );
  }

  public void bind() {
    glBindTexture(GL_TEXTURE_2D, this.textureID);
  }

  public void unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
  }

  protected void delete() {
    glDeleteTextures(this.textureID);
  }
}
