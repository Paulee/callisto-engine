# Callisto2D
Callisto ist eine simple 2D-Game-Engine die in Java 14 geschrieben wurde.
Wie schon gesagt, Callisto ist eine sehr einfach gehaltene Engine, die lediglich dazu da ist, den Benutzer die Entwicklung seines Spiels zu simplifizieren.

### Zu Callisto selbst:
Es war zu Beginn des Projektes eine Option mich mehr mit dem Thema "Spieleentwicklung" und den dazugehörigen Themen zu beschäftigen. Jetzt, nach einiger Zeit ist mir aufgefallen, dass Ich mit dem Release vielleicht mehr Menschen für das Thema begeistern kann und hoffe, dass Callisto dir hilft dein Spiel zu entwickeln.

### Was du wissen solltest:
Es ist ein privates Projekt und dient dazu, mich mehr mit OpenGL etc. vertraut zu machen. Falls die Engine also an enigen Stellen fehlerhaft sein sollte oder ihr einfach Verbesserungsvorschläge habt, könnt ihr mir einfach per Discord schreiben.

Viel Spaß beim Entwickeln mit Callisto ;)
P.S. falls ihr ein Spiel damit erarbeitet habt, könnt ihr mir auch schreiben und ich werde es hier verlinken, so ihr dies wollt.

#### Discord: Paulee\#1284 

### Credits:
   - Designer: Mario_N#1447 auf Discord